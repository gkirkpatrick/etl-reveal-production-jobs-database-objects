﻿

CREATE   PROCEDURE [de].[procImport_CreateSecondaryRecords](
	@TripImportIDs AS IDIntList READONLY)
AS
BEGIN

	--Secondary Records:
		--Locations
		--Customers
		--CancelReasons
		--PassengerTypes
		--Services
		--FareTypes
		--OverrideTypes


	--Use the System UserID for all Edits
	DECLARE @SystemUserID INT = (SELECT [Value] FROM ClientSettings WHERE ClientSettingID = 152)


	--Mapping Table for Trip being Imported with Current Reveal Data
	PRINT 'Create Mapping Table: ' + CONVERT(VARCHAR(30), GETDATE(), 121)
	DECLARE @ImportMapping TABLE
	(
		TripImportID INT,
		CurrentMfTripID INT NULL,
		CurrentUnassignedMfID INT NULL,
		ProviderID INT NULL,
		CustomerID INT NULL,
		GenderID INT NULL,
		PassengerTypeID INT NULL,
		CancelReasonID INT NULL,
		PickLocationID INT NULL,
		DropLocationD INT NULL,
		FundingTypeID INT NULL,
		ServiceID INT NULL,
		TripPurposeID INT NULL,
		OverrideTypeID INT NULL
	)
	INSERT INTO @ImportMapping
		SELECT
			ti.TripImportID,
			de.Import_GetCurrentMfTripID(ti.[Date], ti.ProviderTripID) AS [CurrentMfTripID],
			dbo.GetCurrentUnassignedMfID(ti.[Date]) AS [CurrentUnassignedMfID],
			de.Import_GetProviderID(ti.ProviderCode) AS [ProviderID],
			de.Import_GetCustomerID(ti.Customer, ti.CustomerDoB, ti.CustomerNumber) AS [CustomerID],
			de.Import_GetGenderID(ti.Gender) AS [GenderID],
			de.Import_GetPassengerTypeID(ti.PassengerType) AS [PassengerTypeID],
			de.Import_GetCancelReasonID(ti.CancelReason) AS [CancelReasonID],
			de.Import_GetLocationID(ti.PickCommonName, ti.PickAddress1, ti.PickAddress2, ti.PickCity, ti.PickState, ti.PickZip, ti.PickLatitude, ti.PickLongitude) AS [PickLocationID],
			de.Import_GetLocationID(ti.DropCommonName, ti.DropAddress1, ti.DropAddress2, ti.DropCity, ti.DropState, ti.DropZip, ti.DropLatitude, ti.DropLongitude) AS [DropLocationID],
			de.Import_GetFundingTypeID(ti.FundingType) AS [FundingTypeID],
			de.Import_GetServiceID(ti.[Service]) AS [ServiceID],
			de.Import_GetTripPurposeID(ti.TripPurpose) AS [TripPurposeID],
			de.Import_GetOverrideTypeID(ti.OverrideType) AS [OverrideTypeID]
		FROM de.TripImports ti INNER JOIN @TripImportIDs id ON id.ID = ti.TripImportID


	--LOCATIONS
	PRINT 'Locations: ' + CONVERT(VARCHAR(30), GETDATE(), 121)
	INSERT INTO Locations  ([Description], ImportDescription, Address1, Address2, City, [State], Zip, Latitude, Longitude, OverrideImport, Active,
							CurrentLocationID, UserID, CreatedTime_UTC, GeoCodeTypeID, ManifestNote, CommonName)
		--These two are both the same, Description AND ImportDescription get the same text to start with
		SELECT
			CASE 
				WHEN nl.CommonName != '' 
					THEN '['+nl.CommonName+'] '
				ELSE ''
			END + 
			nl.Address1 + ' ' + 
			CASE
				WHEN nl.Address2 != ''
					THEN nl.Address2 + ' '
				ELSE ''
			END +
			nl.City + ', ' + nl.[State] + ' ' + nl.Zip AS [Description],

			CASE 
				WHEN nl.CommonName != '' 
					THEN '['+nl.CommonName+'] '
				ELSE ''
			END + 
			nl.Address1 + ' ' + 
			CASE
				WHEN nl.Address2 != ''
					THEN nl.Address2 + ' '
				ELSE ''
			END +
			nl.City + ', ' + nl.[State] + ' ' + nl.Zip AS [ImportDescription],

			nl.Address1 AS [Address1],
			nl.Address2 AS [Address2],
			nl.City,
			nl.[State],
			nl.Zip,
			nl.Latitude,
			nl.Longitude,
			1 AS [OverrideImport],
			1 AS [Active],
			0 AS [CurrentLocationID],
			@SystemUserID AS UserID,
			GETUTCDATE() AS [CreatedTime_UTC],
			--CASE WHEN nl.Latitude = 0 THEN 4 ELSE 6 END AS [GeoCodeTypeID],
			4 as [GeoCodeTypeID],
			nl.ManifestNote,
			nl.CommonName
		FROM (
				--Pickup Location
				SELECT DISTINCT 
					ISNULL(ti.PickCommonName,'') AS [CommonName],
					ISNULL(ti.PickAddress1, 'N/A') AS [Address1],
					ISNULL(ti.PickAddress2, '') AS [Address2],
					ISNULL(ti.PickCity,'N/A') AS [City], 
					ISNULL(ti.PickState,'N/A') AS [State], 
					ISNULL(ti.PickZip,'N/A') AS [Zip], 
					0.0 as [Latitude],
					0.0 as [Longitude],
					--ISNULL(ti.PickLatitude ,0.0)as [Latitude],
					--ISNULL(ti.PickLongitude,0.0) AS [Longitude], 
					ISNULL(ti.PickNote,'') AS [ManifestNote]
				FROM de.TripImports ti INNER JOIN @ImportMapping im ON im.TripImportID = ti.TripImportID
				WHERE im.PickLocationID IS NULL
				
				Union 

				--Dropoff Location
				SELECT DISTINCT 
					ISNULL(ti.DropCommonName,'') AS [CommonName],
					ISNULL(ti.DropAddress1, 'N/A') AS [Address1],
					ISNULL(ti.DropAddress2, '') AS [Address2],
					ISNULL(ti.DropCity,'N/A') AS [City], 
					ISNULL(ti.DropState,'N/A') AS [State], 
					ISNULL(ti.DropZip,'N/A') AS [Zip], 
					0.0 as [Latitude],
					0.0 as [Longitude],
					--ISNULL(ti.DropLatitude ,0.0)as [Latitude],
					--ISNULL(ti.DropLongitude,0.0) AS [Longitude], 
					ISNULL(ti.DropNote,'') AS [ManifestNote]
				FROM de.TripImports ti INNER JOIN @ImportMapping im ON im.TripImportID = ti.TripImportID
				WHERE im.DropLocationD IS NULL
			) nl


	--PASSENGER TYPES
	Print 'PassengerTypes: ' + Convert(VARCHAR(30), GetDate(), 121)
	INSERT INTO PassengerTypes (Description, ImportedID, IsWheelChair, CorePassengerTypeID, LastEdit_UTC, LastEditUserID)
		SELECT DISTINCT 
			ti.PassengerType AS [Description], 
			0 AS [ImportedID], 
			CASE WHEN ti.PassengerType like '%WC%' THEN 1 WHEN ti.PassengerType like '%WheelChair%' THEN 1 ELSE 0 END AS [IsWheelchair],
			CASE WHEN ti.PassengerType like '%WC%' THEN 2 WHEN ti.PassengerType like '%WheelChair%' THEN 2 ELSE 1 END AS [CorePassengerTypeID],
			GETUTCDATE() AS [LastEdit_UTC],
			@SystemUserID AS [LastEditUserID]
		FROM de.TripImports ti INNER JOIN @ImportMapping im ON im.TripImportID = ti.TripImportID
		WHERE im.PassengerTypeID IS NULL


	--CUSTOMERS
	Print 'Insert New Customers: ' + Convert(VARCHAR(30), GetDate(), 121)
	INSERT INTO dbo.Customers ([Description], ImportedID, BirthDate, IsVerified, CustomerNumber, ProviderCustomerNumber, GenderID, ProviderID,FirstName,LastName,SSN)
		SELECT DISTINCT 
			ti.Customer AS [Description],
			0 AS [ImportedID], 
			ISNULL(ti.CustomerDoB,'1/1/1800') AS [BirthDate],
			0 AS [IsVerified], 
			CASE ISNULL(ti.CustomerNumber,'') WHEN '' THEN 'N/A' ELSE ti.CustomerNumber END AS [CustomerNumber],
			ti.CustomerNumber AS [ProviderCustomerNumber], 
			im.GenderID AS [GenderID],
			im.ProviderID AS [ProviderID],
			LTRIM(RTRIM(Substring(customer, Charindex(',', customer)+1, LEN(customer)))) as FirstName,
			LTRIM(RTRIM(Substring(customer, 1,Charindex(',', customer)-1))) as  Lastname,
			TI.SSN
		FROM de.TripImports ti INNER JOIN @ImportMapping im ON im.TripImportID = ti.TripImportID
		WHERE im.CustomerID IS NULL

	PRINT 'Update Existing Customers: ' + CONVERT(VARCHAR(30), GETDATE(), 121)
	UPDATE Customers SET GenderID = im.GenderID
	                    ,FirstName = LTRIM(RTRIM(Substring(customer, Charindex(',', customer)+1, LEN(customer))))
						,LastName = LTRIM(RTRIM(Substring(customer, 1,Charindex(',', customer)-1)))
	FROM Customers c INNER JOIN @ImportMapping im ON im.CustomerID = c.CustomerID
	                 INNER JOIN de.TripImports ti ON ti.TripImportID = im.TripImportID
	
	--Update Customer Mapping for Contacts
	PRINT 'Update Mapping with new Customers: ' + CONVERT(VARCHAR(30), GETDATE(), 121)
	UPDATE @ImportMapping SET CustomerID = de.Import_GetCustomerID(ti.Customer, ti.CustomerDoB, ti.CustomerNumber)
	FROM @ImportMapping im INNER JOIN de.TripImports ti ON ti.TripImportID = im.TripImportID

	--CUSTOMER CONTACTS
	Print 'Insert New Primary Customer Contacts; ' + Convert(VARCHAR(30), GetDate(), 121)
	INSERT INTO CustomerContacts (CustomerContactTypeID, CustomerID, [Order], [Description], Active, Created_UTC)
		SELECT DISTINCT 
			1 AS [CustomerContactTypeID], --Phone
			im.CustomerID AS [CustomerID],
			1 AS [Order], --Primary contact
			ISNULL(ti.CustomerPrimaryPhone,'') AS [Description],
			1 AS [Active],
			GETUTCDATE() AS [Created_UTC]
		FROM de.TripImports ti INNER JOIN @ImportMapping im ON im.TripImportID = ti.TripImportID
		WHERE 
			NOT EXISTS (SELECT 1 FROM CustomerContacts icc WHERE icc.CustomerID = im.CustomerID AND [Order] = 1 AND Active = 1) 
			AND im.CustomerID IS NOT NULL

	Print 'Update Invalid Primary Customer Contacts: ' + Convert(VARCHAR(30), GetDate(), 121)
	UPDATE CustomerContacts SET [Description] = ISNULL(u.UpdatedPhone,''), Created_UTC = GETUTCDATE()
		FROM CustomerContacts cc
			INNER JOIN (
				SELECT DISTINCT 
					im.CustomerID AS [CustomerID],
					ISNULL(ti.CustomerPrimaryPhone,'') AS [UpdatedPhone]
				FROM de.TripImports ti INNER JOIN @ImportMapping im ON im.TripImportID = ti.TripImportID
			) u on cc.CustomerID = u.CustomerID AND cc.Active = 1 AND cc.[Order] = 1
		WHERE cc.[Description] <> u.UpdatedPhone

	Print 'Insert New Secondary Customer Contacts; ' + Convert(VARCHAR(30), GetDate(), 121)
	INSERT INTO CustomerContacts (CustomerContactTypeID, CustomerID, [Order], [Description], Active, Created_UTC)
		SELECT DISTINCT 
			1 AS [CustomerContactType], --Phone
			im.CustomerID AS [CustomerID],
			2 AS [Order], --Secondary contact
			ISNULL(ti.CustomerSecondaryPhone,'') AS [Description],
			1 AS [Active],
			GETUTCDATE() AS [Created_UTC]
		FROM de.TripImports ti INNER JOIN @ImportMapping im ON im.TripImportID = ti.TripImportID
		WHERE 
			NOT EXISTS (SELECT 1 FROM CustomerContacts icc WHERE CustomerID = im.CustomerID AND [Order] = 2 AND Active = 1)
			AND im.CustomerID IS NOT NULL

	Print 'Update Invalid Secondary Customer Contacts: ' + Convert(VARCHAR(30), GetDate(), 121)
	UPDATE CustomerContacts SET [Description] = ISNULL(u.UpdatedPhone,''), Created_UTC = GETUTCDATE()
		FROM CustomerContacts cc
			inner join (
				SELECT DISTINCT 
					im.CustomerID AS [CustomerID],
					ISNULL(ti.CustomerSecondaryPhone,'') AS [UpdatedPhone]
				FROM de.TripImports ti INNER JOIN @ImportMapping im ON im.TripImportID = ti.TripImportID
			) u on cc.CustomerID = u.CustomerID AND cc.Active = 1 AND cc.[Order] = 2
		WHERE cc.[Description] <> u.UpdatedPhone


	--CANCEL REASONS
	Print 'CancelReasons: ' + Convert(VARCHAR(30), GetDate(), 121)
	INSERT INTO CancelReasons ([Description], NoShow, LateCancel, ImportedID, LastEdit_UTC, LastEditUserID)
		SELECT DISTINCT 
			ti.CancelReason AS [Description], 
			CASE WHEN ti.CancelReason like 'No Show%' THEN 1 ELSE 0 END AS [NoShow],
			CASE WHEN ti.CancelReason like 'Late%' THEN 1 ELSE 0 END AS [LateCancel],
			0 AS [ImportedID],
			GETUTCDATE() AS [LastEdit_UTC],
			@SystemUserID AS [LastEditUserID]
		FROM de.TripImports ti INNER JOIN @ImportMapping im ON im.TripImportID = ti.TripImportID
		WHERE ti.IsCancelled = 1 AND im.CancelReasonID IS NULL


	--SERVICES
	Print 'Services: ' + Convert(VARCHAR(30), GetDate(), 121)
	INSERT INTO [Services] ([Description], LastEdit_UTC, LastEditUserID)
		SELECT DISTINCT 
			ti.[Service] AS [Description], 
			GETUTCDATE() AS [LastEdit_UTC], 
			@SystemUserID AS [LastEditUserID]
		FROM de.TripImports ti INNER JOIN @ImportMapping im ON im.TripImportID = ti.TripImportID
		WHERE im.ServiceID IS NULL


	--FUNDING TYPES
	Print 'FundingTypes: ' + Convert(VARCHAR(30), GetDate(), 121)
	INSERT INTO FundingTypes ([Description], LastEdit_UTC, LastEditUserID)
		SELECT DISTINCT 
			ti.FundingType AS [Description], 
			GETUTCDATE() AS [LastEdit_UTC], 
			@SystemUserID AS [LastEditUserID]
		FROM de.TripImports ti INNER JOIN @ImportMapping im ON im.TripImportID = ti.TripImportID
		WHERE im.FundingTypeID IS NULL


	--TRIP PURPOSES
	Print 'TripPurposes: ' + Convert(VARCHAR(30), GetDate(), 121)
	INSERT INTO TripPurposes ([Description], Active, SortOrder, ArgbColor, LastEdit_UTC, LastEditUserID)
		SELECT DISTINCT 
			ti.TripPurpose AS [Description],
			1 AS [Active],
			0 AS [SortOrder],
			-1 AS [ArgbColor],
			GETUTCDATE() AS [LastEdit_UTC],
			@SystemUserID AS [LastEditUserID]
		FROM de.TripImports ti INNER JOIN @ImportMapping im ON im.TripImportID = ti.TripImportID
		WHERE im.TripPurposeID IS NULL


	--OVERRIDE TYPES
	Print 'OverrideTypes: ' + Convert(VARCHAR(30), GetDate(), 121)
	INSERT INTO OverrideTypes ([Description], LastEdit_UTC, LastEditUserID)
		SELECT DISTINCT 
			ti.OverrideType AS [Description], 
			GETUTCDATE() AS [LastEdit_UTC], 
			@SystemUserID AS [LastEditUserID]
		FROM de.TripImports ti INNER JOIN @ImportMapping im ON im.TripImportID = ti.TripImportID
		WHERE im.OverrideTypeID IS NULL

	Print 'Done: ' + Convert(VARCHAR(30), GetDate(), 121)
end