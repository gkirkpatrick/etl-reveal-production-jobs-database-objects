﻿


CREATE                               
PROCEDURE [etl].[ProcImport_LoadTripImports_Hamilton_Raw] @FileName varchar(255)
AS
SET NOCOUNT ON
/*********************************************************************
--* ======================================================================
--*  Author: Gene Kirkpatrick 
--*  Date Created: 9/30/2021
--*  VSS: 
--*=======================================================================
*
*	File Name:	
*
*	Description: Import Trips Data from ETL.Hamilton_Trips_Raw table. 
 .
*			
*	PARAMETERS: 
*
*	Change Log (Date, Name and Ddescription)
*
*	--------------------------------------------------------
*	Modification History:  5/9/22 Gene Kirkpatrick, Change https://mtminc.atlassian.net/browse/LS-1956
*********************************************************************/

DECLARE 
      @RowCount	 VARCHAR(255) = ''
		



BEGIN TRY
    BEGIN TRANSACTION


	SELECT
	       
		    providertripid = Concat(Ltrim(rtrim([TRIP_ID])),'   ',Rtrim(Ltrim([TRIP_LEG_ID])))
			,de.Import_GetCurrentMfTripID(cast([TRIP_DATE] as date), Concat(Ltrim(rtrim([TRIP_ID])),'   ',Rtrim(Ltrim([TRIP_LEG_ID])))) AS [CurrentMfTripID]

			 ,[ProviderCode]				 =CASE WHEN LTRIM(RTRIM([TRIP_STATUS])) = 'CHANGED' AND p1.Description = 'MedaCare' THEN 'MC'
	                                                WHEN LTRIM(RTRIM([TRIP_STATUS])) = 'CHANGED' AND p1.Description  = 'Ride Right' THEN 'RR' 
													 WHEN DATEname(DW, lTRIM(RTRIM(TRIP_DATE))) IN ('Saturday','Sunday') THEN 'MC'
                                                      WHEN DATEDIFF(hour,CAST(DOB AS date),cast([TRIP_DATE] as date))/ 8766 <= 17 AND (SELECT left(trim(WORKER_NAME),1) ) = 1 THEN 'RR'
                                                       WHEN (DATEDIFF(HOUR,CAST(ISNULL(nullif([DOB],''),'1/1/1900')AS date),cast([TRIP_DATE] as date))/ 8766 >= 18 and ISNULL(nullif([DOB],''),'1/1/1900') NOT IN ('1/1/1900'))  THEN 'MC'
						                         	    WHEN ([TRIP_PURPOSE]LIKE '%day%' OR [TRIP_PURPOSE]LIKE '%php%')    THEN  'RR'
														 WHEN [TRIP_PURPOSE]LIKE '%prs%'   THEN 'MC'
						                         	      WHEN right(rtrim([ETL].[ParseAddress]([DROPOFF_ADDRESS], N'City')),charindex(' ',reverse(rtrim([ETL].[ParseAddress]([DROPOFF_ADDRESS], N'City')))+' ')-1) = 'Dayton' THEN 'MC'
						                         	       WHEN (SELECT left(trim(WORKER_NAME),1) ) >= 2 THEN 'MC'
														   ELSE 'PU'
														    END
           ,[PassengerType]    =  CASE WHEN HR.MOBILITY_AIDE LIKE '%CAR SEAT%' THEN  'CAR SEAT'
	                                     WHEN HR.MOBILITY_AIDE LIKE '%BOOSTER SEAT%' THEN  'BOOSTER'
					                       WHEN HR.MOBILITY_AIDE LIKE '%L-SHAPED SEAT%' THEN 'L-SHAPE'
										 
						                ELSE 'AMB'
						            END

           ,[Service]       = CASE WHEN HR.[PICKUP_ADDRESS] LIKE '%5400 Edalbert%' OR HR.[DROPOFF_ADDRESS] LIKE '%5400 Edalbert%'THEN 'StJoseph'WHEN HR.[PICKUP_ADDRESS] LIKE '%2850 Winslow%' OR HR.[DROPOFF_ADDRESS] LIKE '%2850 Winslow%'THEN 'TIP'
		                            WHEN HR.[PICKUP_ADDRESS] LIKE '%7140 Office Park%' OR HR.[DROPOFF_ADDRESS] LIKE '%7140 Office Park%'THEN 'PosLeaps'
									  WHEN HR.[PICKUP_ADDRESS] LIKE '%1994 Madison%' OR HR.[DROPOFF_ADDRESS] LIKE '%1994 Madison%'THEN 'GladHouse'
									   WHEN HR.[PICKUP_ADDRESS] LIKE '%274 Sutton%' OR HR.[DROPOFF_ADDRESS] LIKE '%274 Sutton%'THEN 'Altercrest'
									    WHEN HR.[PICKUP_ADDRESS] LIKE '%4600 Smith%' OR HR.[DROPOFF_ADDRESS] LIKE '%4600 Smith%'THEN 'Camelot'
										 WHEN HR.[PICKUP_ADDRESS] LIKE '%5014 Madison%' OR HR.[DROPOFF_ADDRESS] LIKE '%5014 Madison%'THEN 'CH5014'
										  WHEN HR.[PICKUP_ADDRESS] LIKE '%3325 Glenmore%' OR HR.[DROPOFF_ADDRESS] LIKE '%3325 Glenmore%'THEN 'Beech'
										   WHEN HR.[PICKUP_ADDRESS] LIKE '%1500 Mcmakin%' OR HR.[DROPOFF_ADDRESS] LIKE '%1500 Mcmakin%'THEN 'Assumption'
										    WHEN HR.[PICKUP_ADDRESS] LIKE '%249 W Forrer%' OR HR.[DROPOFF_ADDRESS] LIKE '%249 W Forrer%' OR HR.[PICKUP_ADDRESS] LIKE '%249 W. Forrer%' OR HR.[DROPOFF_ADDRESS] LIKE '%249 W. Forrer%' OR HR.[PICKUP_ADDRESS] LIKE '%249 West Forrer%' OR HR.[DROPOFF_ADDRESS] LIKE '%249 West Forrer%'THEN 'Lockland'
											 WHEN HR.[PICKUP_ADDRESS] LIKE '%4721 Reading%' OR HR.[DROPOFF_ADDRESS] LIKE '%4721 Reading%'THEN 'StAls'
											  WHEN HR.[PICKUP_ADDRESS] LIKE '%5050 Madison%' OR HR.[DROPOFF_ADDRESS] LIKE '%5050 Madison%'THEN 'CH5050'
											   WHEN HR.[PICKUP_ADDRESS] LIKE '%7162 Reading%' OR HR.[DROPOFF_ADDRESS] LIKE '%7162 Reading%'THEN 'FNC'
											    WHEN HR.[PICKUP_ADDRESS] LIKE '%222 E Central%' OR HR.[DROPOFF_ADDRESS] LIKE '%222 E Central%' OR HR.[PICKUP_ADDRESS] LIKE '%222 E. Central%' OR HR.[DROPOFF_ADDRESS] LIKE '%222 E. Central%' OR HR.[PICKUP_ADDRESS] LIKE '%222 East Central%' OR HR.[DROPOFF_ADDRESS] LIKE '%222 East Central%'THEN 'JFS'
												 WHEN HR.[PICKUP_ADDRESS] LIKE '%3900 Cottingham%' OR HR.[DROPOFF_ADDRESS] LIKE '%3900 Cottingham%'THEN 'Princeton'
												  WHEN HR.[PICKUP_ADDRESS] LIKE '%2020 Sherman%' OR HR.[DROPOFF_ADDRESS] LIKE '%2020 Sherman%'THEN 'Norwood'
											ELSE 'MTM'
                               END
        INTO #ImportMapping
		FROM [ETL].Hamilton_Trips_Raw HR
        LEFT JOIN MfTrips mft on de.Import_GetCurrentMfTripID(cast([TRIP_DATE] as date), Concat(Ltrim(rtrim([TRIP_ID])),'   ',Rtrim(Ltrim([TRIP_LEG_ID])))) = mft.MfTripID
        LEFT JOIN providers p1 on mft.ProviderID  = p1.ProviderID



	
 INSERT INTO [de].[TripImports]([Date],[ProviderCode],[ProviderTripID],[ProviderDistance],[ProviderCost],[Customer],[CustomerDoB],[CustomerNumber],[CustomerPrimaryPhone],[CustomerSecondaryPhone]
                                               ,[Gender],[PassengerType],[IsCancelled],[CancelReason],[PickCommonName],[PickAddress1],[PickAddress2],[PickCity],[PickState],[PickZip],[PickLatitude],[PickLongitude]
                                               ,[PickNote],[DropCommonName],[DropAddress1],[DropAddress2],[DropCity],[DropState],[DropZip],[DropLatitude],[DropLongitude],[DropNote],[FundingType],[FareAmount]
                                               ,[Service],[TripPurpose],[OverrideType],[PickWindowBegin],[PickWindowEnd],[DropWindowBegin],[DropWindowEnd],[Attendants],[Guests],[TripNote],[SpecialNeeds]
                                               ,[IsWillCall],[Unassign],[OverrideUnassign],[ImportStatusID],[ImportFile],[Imported_UTC],[ImportedByUserID],[IsPrimaryReviewed],[PrimaryApproval_UTC],[PrimaryApprovalByUserID]
                                               ,[PrimaryRejection_UTC],[PrimaryRejectionByUserID],[PrimaryRejectionReason],[IsSecondaryReviewed],[SecondaryApproval_UTC],[SecondaryApprovalByUserID],[SecondaryRejection_UTC]
                                                      ,[SecondaryRejectionByUserID],[SecondaryRejectionReason],[LastEdit_UTC],[LastEditUserID],[SSN] --,[First Name],[Last Name]
													  )




SELECT
      -- [TripImportID]             			 = [TRIP_ID]
       [Date]								 = cast([TRIP_DATE] as date)
   
      ,[ProviderCode]						 = im.providercode
      ,[ProviderTripID]						 = Concat(Ltrim(rtrim([TRIP_ID])),'   ',Rtrim(Ltrim([TRIP_LEG_ID])))
      ,[ProviderDistance]					 = 0.0	
      ,[ProviderCost]						 = P.FixedProviderCost
      ,[Customer]							 = Ltrim(Rtrim([LAST_NAME])) + ', ' + LTRIM(RTRIM([FIRST_NAME])) 
      ,[CustomerDoB]						 = ISNULL([DOB],'1/1/1900')
      ,[CustomerNumber]						 = CONCAT(Left(LTRIM(UPPER([FIRST_NAME])),2),Left(ltrim(UPPER([LAST_NAME])),2),Right(rtrim(SSN),5))
      ,[CustomerPrimaryPhone]				 = LTRIM(RTRIM([CLIENT_PHONE_NO]))
      ,[CustomerSecondaryPhone]				 = 0
      ,[Gender]								 = 'N/A' 
      ,[PassengerType]						 = im.PassengerType
      ,[IsCancelled]						 =  CASE WHEN Ltrim(rtrim([TRIP_STATUS]))  =  'CANCELED' THEN 1 ELSE 0 END  
      ,[CancelReason]						 = ''
      ,[PickCommonName]						 = '' -- LTRIM(RTRIM([PICKUP_PROVIDER]))
      ,[PickAddress1]						 = SUBSTRING([ETL].[ParseAddress]([PICKUP_ADDRESS], N'City'), 1, LEN([ETL].[ParseAddress]([PICKUP_ADDRESS], N'City')) - CHARINDEX(' ', REVERSE([ETL].[ParseAddress]([PICKUP_ADDRESS], N'City')))) 
      ,[PickAddress2]						 = ''
      ,[PickCity]							 = right(rtrim([ETL].[ParseAddress]([PICKUP_ADDRESS], N'City')),charindex(' ',reverse(rtrim([ETL].[ParseAddress]([PICKUP_ADDRESS], N'City')))+' ')-1)
      ,[PickState]							 = [ETL].[ParseAddress]([PICKUP_ADDRESS], N'State') 
      ,[PickZip]							 = [ETL].[ParseAddress]([PICKUP_ADDRESS], N'Zip')
      ,[PickLatitude]						 = 0.00
      ,[PickLongitude]						 = 0.00
      ,[PickNote]							 = null
      ,[DropCommonName]						 = NULL --[DROPOFF_PROVIDER]
      ,[DropAddress1]						 = SUBSTRING([ETL].[ParseAddress]([DROPOFF_ADDRESS], N'City'), 1, LEN([ETL].[ParseAddress]([DROPOFF_ADDRESS], N'City')) - CHARINDEX(' ', REVERSE([ETL].[ParseAddress]([DROPOFF_ADDRESS], N'City')))) 
      ,[DropAddress2]						 = ''
      ,[DropCity]							 = right(rtrim([ETL].[ParseAddress]([DROPOFF_ADDRESS], N'City')),charindex(' ',reverse(rtrim([ETL].[ParseAddress]([DROPOFF_ADDRESS], N'City')))+' ')-1)
      ,[DropState]							 = [ETL].[ParseAddress]([DROPOFF_ADDRESS], N'State') 
      ,[DropZip]							 = [ETL].[ParseAddress]([DROPOFF_ADDRESS], N'Zip')
      ,[DropLatitude]						 = 0.00
      ,[DropLongitude]						 = 0.00
      ,[DropNote]							 = ''
      ,[FundingType]						 = 'N/A'
      ,[FareAmount]							 = 0.00
      ,[Service]							 = im.Service
      ,[TripPurpose]						 = LTRIM(RTRIM([TRIP_PURPOSE]))
      ,[OverrideType]						 = 'N/A'
   
	 ,[PickWindowBegin]					 =  CASE WHEN (CASE WHEN NULLIF([RETURN_TIME],'') IS NOT NULL THEN 'F' ELSE 'T' END) = 'F' 
	                                                THEN CAST([TRIP_DATE]+' '+[RETURN_TIME] AS DATETIME )
													              ELSE CAST([TRIP_DATE]  AS DATETIME)
																    END
                                                    
      ,[PickWindowEnd]						 = CASE WHEN (CASE WHEN NULLIF([RETURN_TIME],'') IS NOT NULL THEN 'F' ELSE 'T' END) = 'F' 
	                                                 THEN CASE WHEN CAST([RETURN_TIME] AS TIME ) IN ('23:59','23:57','23:58')
													            THEN CAST([TRIP_DATE]+' '+[RETURN_TIME] AS DATETIME )
													              ELSE CAST([TRIP_DATE] + ' ' + DATEADD(minute,20, [RETURN_TIME]) AS DATETIME)
																    END
                                                     ELSE DATEADD(hour,48,CAST([TRIP_DATE] AS DATETIME))
													END
      ,[DropWindowBegin]					 = CASE WHEN (CASE WHEN NULLIF([RETURN_TIME],'') IS NOT NULL THEN 'F' ELSE 'T' END) = 'T' 
	                                                THEN (CASE WHEN im.providercode = 'MC' THEN CAST([TRIP_DATE] + ' ' + DATEADD(minute,-20,ISNULL(LEFT(CAST(Convert(Time,[APPT_TIME]) as varchar),5),'23:59') ) as datetime)
													             WHEN im.providercode = 'RR' THEN CAST([TRIP_DATE] + ' ' + DATEADD(minute,-10,ISNULL(LEFT(CAST(Convert(Time,[APPT_TIME]) as varchar),5),'23:59') ) as datetime)    
															       END )
													ELSE CAST([TRIP_DATE]  AS datetime)
													END

      ,[DropWindowEnd]						 = CASE WHEN (CASE WHEN NULLIF([RETURN_TIME],'') IS NOT NULL THEN 'F' ELSE 'T' END) = 'T' 
	                                                THEN (CASE WHEN im.providercode = 'MC' THEN  CAST([TRIP_DATE] + ' ' +ISNULL(LEFT(CAST(Convert(Time,[APPT_TIME]) as varchar),5),'23:59')  as datetime)
													             WHEN im.providercode = 'RR' THEN CAST([TRIP_DATE] + ' ' + DATEADD(minute,+10,ISNULL(LEFT(CAST(Convert(Time,[APPT_TIME]) as varchar),5),'23:59') ) as datetime) 
															      END)
													ELSE DATEADD(hour,48,CAST([TRIP_DATE] AS DATETIME))
													END
      ,[Attendants]							 = 0
      ,[Guests]								 = Case when [NO_RIDERS] > 1 THEN ([NO_RIDERS] - 1) ELSE [NO_RIDERS] END
      ,[TripNote]							 = NULL
      ,[SpecialNeeds]						 = [MOBILITY_AIDE]
      ,[IsWillCall]							 = CASE WHEN [APPT_TIME] IN ('23:59','11:59 pm','11:58 pm','11:57 pm','23:57','23:58')THEN 1  
	                                                 WHEN [RETURN_TIME] IN ('23:59','11:59 pm','11:58 pm','11:57 pm','23:57','23:58')THEN 1 
													 WHEN NULLIF([APPT_TIME],'') IS NULL  AND NULLIF([RETURN_TIME],'') IS NULL   THEN 1
													 ELSE 0

												   END
      ,[Unassign]							 = 0
      ,[OverrideUnassign]					 = 0
      ,[ImportStatusID]						 = 2
      ,[ImportFile]							 = @FileName
      ,[Imported_UTC]						 = GETUTCDATE()
      ,[ImportedByUserID]					 = 11
      ,[IsPrimaryReviewed]					 = 0
      ,[PrimaryApproval_UTC]				 = NULL
      ,[PrimaryApprovalByUserID]			 = NULL
      ,[PrimaryRejection_UTC]				 = NULL
      ,[PrimaryRejectionByUserID]			 = NULL
      ,[PrimaryRejectionReason]				 = NULL
      ,[IsSecondaryReviewed]				 = 0
      ,[SecondaryApproval_UTC]				 = NULL
      ,[SecondaryApprovalByUserID]			 = NULL
      ,[SecondaryRejection_UTC]				 = NULL
      ,[SecondaryRejectionByUserID]			 = NULL
      ,[SecondaryRejectionReason]			 = NULL
      ,[LastEdit_UTC]						 = Dateadd(MINUTE,5,GETUTCDATE())
      ,[LastEditUserID]                      = 6
	  ,[SSN]                                 = ISNULL(Ltrim(Rtrim(HR.SSN)),'0')


  
  FROM [ETL].Hamilton_Trips_Raw HR
  LEFT JOIN #ImportMapping im on  Concat(Ltrim(rtrim(HR.[TRIP_ID])),'   ',Rtrim(Ltrim([TRIP_LEG_ID])))  = IM.providertripid
  LEFT JOIN MfTrips mft       on  im.CurrentMfTripID = mft.MfTripID
  LEFT JOIN providers p1      on  mft.ProviderID  = p1.ProviderID
  LEFT JOIN dbo.Providers P   on  P.Description = CASE WHEN IM.ProviderCode = 'MC' THEN 'MedaCare'
                                                       WHEN IM.ProviderCode = 'RR' THEN 'Ride Right' 
													   END
  WHERE [TRIP_STATUS] <> 'CALLED IN'



  -- Clean up
  DROP TABLE #ImportMapping
  IF object_id(N'#ImportMapping', N'U') IS NOT NULL  DROP TABLE #ImportMapping



SET @RowCount = CONCAT(@RowCount,'[de].[TripImports]: ', CAST(@@ROWCOUNT AS VARCHAR));
 
SELECT @RowCount,0,NULL; 

 COMMIT TRANSACTION
END TRY 
BEGIN CATCH
	IF @@ERROR > 0
	 ROLLBACK TRANSACTION;

	SELECT NULL,-1, CONCAT('ErrorLine: ',ERROR_LINE(),', Error: ',ERROR_MESSAGE());

END CATCH;
GO


