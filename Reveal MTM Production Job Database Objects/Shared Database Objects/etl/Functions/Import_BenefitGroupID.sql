﻿




CREATE   FUNCTION [etl].[Import_BenefitGroupID]
 (@GroupCode varchar(255)
 ,@PlanCode varchar(255))

RETURNS int AS
BEGIN 

Declare @BenefitGroupID int

    SELECT  @BenefitGroupID = BG.BenefitGroupID
      FROM dbo.BenefitGroups BG with(nolock)
INNER JOIN ClientPlans CP with(nolock) ON BG.ClientPlanID = CP.ClientPlanID AND CP.PlanCode = @PlanCode
     WHERE BG.GroupCode =  @GroupCode
	   AND BG.Active = 1 
	   AND BG.Deleted_UTC IS NULL
	 


return @BenefitGroupID 

END