﻿


CREATE   FUNCTION [etl].[Import_GetLocationID](
	@CommonName varchar(255),
	@Address1 varchar(255),
	@Address2 varchar(255),
	@City varchar(255),
	@State varchar(255),
	@Zip varchar(255)
	
	)
RETURNS int AS  
BEGIN 

Declare @LocationID as int



Declare @Description as Varchar(255)
Set @Description = (
	Case 
		When ISNULL(@CommonName,'') != '' 
			Then '[' + ISNULL(@CommonName,'') + '] '
		Else ''
	End + @Address1 + ' ' + 
	Case
		When ISNULL(@Address2,'') != ''
			Then ISNULL(@Address2,'') + ' '
		Else ''
	End + @City + ', ' + @State + ' ' + @Zip);



Select top 1 @LocationID = l.LocationID 
From Locations l 
Where l.ImportDescription = @Description and l.active = 1 -- See Ops-1252 
Order by LocationID asc;



IF(@LocationID is null )
Begin

Select top 1 @LocationID = l.LocationID 
From Locations l 
Where l.Address1 = @Address1 
  AND l.Address2 = @Address2
  AND l.City     = @City
  AND L.[State]  = @State
  AND L.Zip      = @Zip
  AND l.active = 1 --and l.Latitude = @Latitude and l.Longitude = @Longitude
Order by LocationID  asc
END

If (@LocationID is null AND @Address1 = '' OR @City = '' OR  @State = '' OR  @Zip = '' )
Begin
Select @LocationID = 0
End

return @LocationID
END