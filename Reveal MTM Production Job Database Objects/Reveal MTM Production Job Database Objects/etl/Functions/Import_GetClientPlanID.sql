﻿

CREATE FUNCTION [ETL].[Import_GetClientPlanID](@PlanCode varchar(255))
RETURNS int AS
BEGIN 

Declare @ClientPlanID int

Select top 1 @ClientPlanID= CP.ClientPlanID
FROM ClientPlans CP with(nolock)

Where CP.PlanCode =  @PlanCode 


return @ClientPlanID

END
