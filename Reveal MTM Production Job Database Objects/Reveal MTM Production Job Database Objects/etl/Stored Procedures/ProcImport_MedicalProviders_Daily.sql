﻿



CREATE PROCEDURE  [etl].[ProcImport_MedicalProviders_Daily]
AS
SET NOCOUNT ON; 
/*********************************************************************
--* ======================================================================
--*  Author:Gene Kirkpatrick
--*  Date Created: 12/21/2020
--*  JIRA: OPS- OPS-1073 & OPS-1072
--*=======================================================================
*
*	
*
*	Description: Import daily MedicalProviders from SFTP. Data from file is imported into [etl].[MedicalProviders_DailyImport_Stage] table
                 Using SSIS MedicalProviders_Daily.

*			
*	PARAMETERS: 
*
*	Change Log (Date, Name and Ddescription)
*
*	--------------------------------------------------------
*	Modification History: 
*   2/12/2021 , Gene Kirkpatrick, JIRA OPS-1148
*   5/6/2021 , Gene Kirkpatrick, JIRA OPS-1463  
   11/28/2022 , Gene Kirkpatrick, JIRA OPS-3706
*********************************************************************/

 
             -- Row Count for ETL log
         DECLARE @Locationrow Varchar(255)
				, @MedicalProviderInsertRowCount as int = 0
				 ,@MedicalProviderUpdateRowCount as int = 0
				 ,@DuplicateSourcID Varchar(255)
				 ,@NoSourceID as int =0





--- Remove Duplicate SourcID from file

DELETE  FROM [etl].[MedicalProviders_DailyImport_Stage]
WHERE ID IN(

  SELECT ID
    FROM (
            SELECT 
                  ID, 
                  SourceiD,
                  ROW_NUMBER() OVER ( PARTITION BY SourceID ORDER BY ID ) row_num
              FROM 
                  [etl].[MedicalProviders_DailyImport_Stage]
    
           ) as A

    WHERE a.Row_num > 1
		   );

 SET @DuplicateSourcID =  CAST(@@RowCount AS VARCHAR(255));


 --- Remove records with no SourceID --https://mtminc.atlassian.net/browse/OPS-3706

DELETE FROM [etl].[MedicalProviders_DailyImport_Stage] where SourceID is null or SourceID = '';
SET @NoSourceID  = @@ROWCOUNT;

 
 -- Update medical provider Records with a expired end date. OPS-1073
UPDATE M
 SET LastEdit_UTC   = GETUTCDATE()
    ,LastEditUserID = 6 
	,Approved       = 0


--SELECT *
 from dbo.MedicalProviders M
WHERE End_Date < GETUTCDATE()
  AND ClientPlanID = 1
  AND Approved = 1;


--- Update records with blank location data OPS-1463
               UPDATE MS
				   SET ImportFlag = 0

       
	        --select ID 
	            FROM [etl].[MedicalProviders_DailyImport_Stage] MS
				WHERE [ETL].[Import_GetLocationID]('',RTRIM(ISNULL(Address1,'')),RTRIM(ISNULL(Address2, '')),RTRIM(City),RTRIM([State]),RTRIM(Left(Zip,5))) = 0
				  AND ImportFlag = 1
	
	  


  --- End

 BEGIN TRY
-- Import Locations

	 INSERT INTO dbo.Locations  ([Description], ImportDescription, Address1, Address2, City, [State], Zip, Latitude, Longitude, OverrideImport, Active,
	  					CurrentLocationID, UserID, CreatedTime_UTC, GeoCodeTypeID, ManifestNote, CommonName,Notes,LastEdit_UTC)
		--These two are both the same, Description AND ImportDescription get the same text to start with
		SELECT 
		
			CASE 
				WHEN nl.CommonName != '' 
					THEN '['+nl.CommonName+'] '
				ELSE ''
			END + 
			nl.Address1 + ' ' + 
			CASE
				WHEN nl.Address2 != ''
					THEN nl.Address2 + ' '
				ELSE ''
			END +
			nl.City + ', ' + nl.[State] + ' ' + nl.Zip         AS [Description],

			CASE 
				WHEN nl.CommonName != '' 
					THEN '['+nl.CommonName+'] '
				ELSE ''
			END + 
			nl.Address1 + ' ' + 
			CASE
				WHEN nl.Address2 != ''
					THEN nl.Address2 + ' '
				ELSE ''
			END +
			nl.City + ', ' + nl.[State] + ' ' + nl.Zip          AS [ImportDescription]

			, nl.Address1										AS [Address1]
			, nl.Address2										AS [Address2]
			, nl.City
			, nl.[State]
			, nl.Zip
			, nl.Latitude
			, nl.Longitude
			, 1													AS [OverrideImport]
			, 1													AS [Active]
			, 0													AS [CurrentLocationID]
			, 6													AS UserID
			, GETUTCDATE()										AS [CreatedTime_UTC]
			, CASE WHEN nl.Latitude = '0.0' THEN 4 ELSE 6 END 	AS [GeoCodeTypeID]   
			, nl.ManifestNote
			, nl.CommonName  --
			, 'Med Prov Daily Import'									AS [Note]
			, GETUTCDATE()                                      as LastEdit_UTC


		FROM (
				SELECT DISTINCT 
					''							                AS [CommonName]
					, Rtrim(ISNULL(Address1, ''))			    AS [Address1]
					, RTRIM(ISNULL(Address2,''))			    	    AS [Address2]
					, RTrim(City)							    AS [City]
					, RTRIM([State])								    AS [State]
					, RTRIM(LEFT(zip,5))						        AS [Zip] 
					, ISNULL(NULLIF(Latitude,''),'0.0')			AS [Latitude]
					, ISNULL(NULLIF(Longitude,''),'0.0')		AS [Longitude]
					, ISNULL(Notes,'')							AS [ManifestNote]
				
				FROM [etl].[MedicalProviders_DailyImport_Stage]
				WHERE [ETL].[Import_GetLocationID]('',RTRIM(ISNULL(Address1,'')),RTRIM(ISNULL(Address2, '')),RTRIM(City),RTRIM([State]),RTRIM(Left(Zip,5))) IS NULL
			) nl ;

  -- Set @RowCount for the last insert statement 
              SET @Locationrow =  CAST(@@RowCount AS VARCHAR(255));


	
--**** Import MedicalProviders	
 -- Batch insert
DECLARE @r INT;
    SET @r = 1 ; 
	


--- Maping to Update MedicalProviders_Raw ImportFlag after insert and get Row counts for Merge
   DECLARE @Map AS TABLE
    (
          Id int 
		, [Action]  varchar(20)
    );
 

WHILE @r > 0
BEGIN
 
 -- BEGIN TRANSACTION

  delete @Map;


	MERGE  INTO dbo.MedicalProviders AS T
	USING (	
         SELECT 
          
        	(    
        		SELECT TOP 1 ClientPlanID 
        		FROM ClientPlans cp 
        		WHERE cp.plancode = IIF(LEN(S.PlanID)=3,CONCAT(0,s.PlanID),s.PlanID)
        	 )																 	         AS clientPlanID
        	, ISNULL(s.MedicalProviderName,'')							                 AS [Description]
        	, [ETL].[Import_GetLocationID]('',RTRIM(ISNULL(Address1,'')),RTRIM(ISNULL(Address2, '')),RTRIM(City),RTRIM([State]),RTRIM(Left(Zip,5))) AS LocationID
        	, ISNULL(s.NPI,'')	                                                                 AS NPI
        	, ISNULL(s.MedicalProviderID,'')										             AS MedicalProviderCode
        	, ISNULL(s.Specialty,'')					            				             AS MedicalProviderType
        	, s.Phone1																           
        	, s.Phone1ext														                 AS Phone1ext
        	, s.Phone2													                         AS Phone2
        	, s.Phone2ext													                     AS Phone2ext
        	, s.Notes													                         AS Notes
        	,  CASE WHEN ISNULL(NULLIF(S.end_date,''),'9999-12-31') < GETUTCDATE()  
        	          THEN 0 ELSE 1 END	                                                         AS Active
        	,6																                     AS LastEditUserID 
        	, GETUTCDATE()														                 AS LastEdit_UTC
        
        	,CASE WHEN GETUTCDATE() BETWEEN ISNULL(NULLIF(s.[Effective_Date],''),GetUTCDATE())  
        	         AND ISNULL(NULLIF(S.end_date,''),'9999-12-31') THEN 1 ELSE 0 END    AS Approved
        
        	, s.SourceID                                                                         AS ImportID
        	,ISNULL(NULLIF(s.[Effective_Date],''),GetUTCDATE())                                    AS Effective_Date
        	,ISNULL(NULLIF(S.end_date,''),'9999-12-31')                                          AS End_Date
        	,s.ID                                                                                AS ID
          FROM [etl].[MedicalProviders_DailyImport_Stage] S
         WHERE ID IN (SELECT TOP 5000 ID 
                        FROM [etl].[MedicalProviders_DailyImport_Stage] 
                       WHERE ImportFlag = 1 order by ID desc)
		         )AS S
   
	
	 ON  t.importid =  s.importid 
	-- AND s.importid is not null OR s.importid <> ''

	
WHEN MATCHED  THEN
  UPDATE SET          T.[ClientPlanID]	           =  S.[ClientPlanID]	
					, T.[Description]			   =  S.[Description]
	 				, T.[LocationID]			   =  S.[LocationID]	
					, T.[NPI]					   =  S.[NPI]
					, T.[MedicalProviderCode]	   =  S.[MedicalProviderCode]
					, T.[MedicalProviderType]	   =  S.[MedicalProviderType]
					, T.[Phone1]				   =  S.[Phone1]	
					, T.[Phone1Ext]				   =  S.[Phone1Ext]
					, T.[Phone2]				   =  S.[Phone2]	
					, T.[Phone2Ext]				   =  S.[Phone2Ext]
					, T.[Notes]					   =  S.[Notes]
					, T.[Active]				   =  S.[Active]	
					, T.[LastEditUserID]		   =  S.[LastEditUserID]
					, T.[LastEdit_UTC]			   =  S.[LastEdit_UTC]
					, T.[Approved]				   =  S.[Approved]
					, T.[Effective_Date]		   =  S.[Effective_Date]	
					, T.[End_Date]				   =  S.[End_Date]
					





            
	WHEN NOT MATCHED THEN

	INSERT (ClientPlanID, Description, LocationID, NPI, MedicalProviderCode, MedicalProviderType, Phone1, Phone1Ext, Phone2, Phone2Ext
          , Notes, Active, LastEditUserID, LastEdit_UTC,Approved,Effective_Date,End_Date,ImportID)
	VALUES(
          
                      S.[ClientPlanID]	
                    , S.[Description]
                    , S.[LocationID]	
                    , S.[NPI]
                    , S.[MedicalProviderCode]
                    , S.[MedicalProviderType]
                    , S.[Phone1]	
                    , S.[Phone1Ext]
                    , S.[Phone2]	
                    , S.[Phone2Ext]
                    , S.[Notes]
                    , S.[Active]	
                    , S.[LastEditUserID]
                    , S.[LastEdit_UTC]
                    , S.[Approved]
                    , S.[Effective_Date]	
                    , S.[End_Date]
                    , S.[ImportID]	
  		  )

     OUTPUT S.ID, $action   INTO @Map (ID,[Action]);
	 SET @r = @@ROWCOUNT;   
	 SET @MedicalProviderUpdateRowCount  = @MedicalProviderUpdateRowCount + (Select Count(1) from @Map where [Action] = 'UPDATE');
	 SET @MedicalProviderInsertRowCount =  @MedicalProviderInsertRowCount + (Select Count(1) from @Map where [Action] = 'INSERT');

	 


-- Update Raw table Importflag to 0 so we know it is imported
   UPDATE MR
          SET ImportFlag = 0 
         FROM [etl].[MedicalProviders_DailyImport_Stage] MR
   INNER JOIN @Map M on MR.ID = M.ID;


  
   
 
   
--COMMIT TRANSACTION;	



END;

SELECT CONCAT('No SourceID: ',@NoSourceID ,', DuplicateSourceID: ',@DuplicateSourcID,', Locations: ',@Locationrow,', MedicalProvidersInserted: ',Cast(@MedicalProviderInsertRowCount as varchar(255)),', MedicalProvidersUpdated: ',Cast(@MedicalProviderUpdateRowCount as varchar(255))),0,NULL;


END TRY 
BEGIN CATCH
	IF @@ERROR > 0
	-- ROLLBACK TRANSACTION;	 
	 
		SELECT NULL,-1, CONCAT('ErrorLine: ',ERROR_LINE(),', Error: ',ERROR_MESSAGE());
END CATCH;
