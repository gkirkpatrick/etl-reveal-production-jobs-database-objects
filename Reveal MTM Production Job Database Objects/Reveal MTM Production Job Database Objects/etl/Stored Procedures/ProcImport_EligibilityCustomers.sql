﻿




/*
select top 1000 * from etl.Eligibility_Raw where customerid is null

 EXEC [etl].[ProcImport_EligibilityCustomers]


*/ 

CREATE    PROCEDURE  [etl].[ProcImport_EligibilityCustomers]
AS
SET NOCOUNT ON; 
/*********************************************************************
--* ======================================================================
--*  Author:Gene Kirkpatrick
--*  Date Created: 1/29/2021
--*  JIRA: Note Task # for JIRA are in comments example OPS-1011
--*=======================================================================
*
*	
*
*	Description: Import Eligibility Customers 

*			
*	PARAMETERS: 
*
*	Change Log (Date, Name and Ddescription)
*
*	--------------------------------------------------------
*	Modification History:
*********************************************************************/
-------------------------------------------------------------------------------------------------------------


             -- Row Count for ETL log
         DECLARE
	              @RowCount	VARCHAR(255) = ''
		        , @Locationrow  as int  = 0
				, @CustomerInsertRowCount as int = 0
				 ,@CustomerUpdateRowCount as int = 0

				 ,@CustomerCommonLocationsUpdateRowCount as int = 0
				 ,@CustomerCommonLocationsInsertRowCount as int = 0
				 ,@CustomerContactsInsertRowCount as int = 0
				 ,@CustomerPlanEligibilitiesInserted as int = 0
				 ,@CustomerPlanEligibilitiesUpdated as int = 0
				

 BEGIN TRY 


----------------------------------------------------------------------------------------------------------
-- INSERT LOCATIONS  OPS -1011
----------------------------------------------------------------------------------------------------------
 INSERT INTO Locations  ([Description], ImportDescription, Address1, Address2, City, [State], Zip, Latitude, Longitude, OverrideImport, Active,
	  					CurrentLocationID, UserID, CreatedTime_UTC, GeoCodeTypeID, ManifestNote, CommonName,Notes)
	

		SELECT 
		
			CASE 
				WHEN nl.CommonName != '' 
					THEN '['+nl.CommonName+'] '
				ELSE ''
			END + 
			nl.Address1 + ' ' + 
			CASE
				WHEN nl.Address2 != ''
					THEN nl.Address2 + ' '
				ELSE ''
			END +
			nl.City + ', ' + nl.[State] + ' ' + nl.Zip         AS [Description],

			CASE 
				WHEN nl.CommonName != '' 
					THEN '['+nl.CommonName+'] '
				ELSE ''
			END + 
			nl.Address1 + ' ' + 
			CASE
				WHEN nl.Address2 != ''
					THEN nl.Address2 + ' '
				ELSE ''
			END +
			nl.City + ', ' + nl.[State] + ' ' + nl.Zip          AS [ImportDescription]

			, nl.Address1										AS [Address1]
			, nl.Address2										AS [Address2]
			, nl.City
			, nl.[State]
			, nl.Zip
			, nl.Latitude
			, nl.Longitude
			, 1													AS [OverrideImport]
			, 1													AS [Active]
			, 0													AS [CurrentLocationID]
			, 6													AS UserID
			, GETUTCDATE()										AS [CreatedTime_UTC]
			, 0                                                 AS [GeoCodeTypeID]
			--, CASE WHEN nl.Latitude = '0.0' THEN 4 ELSE 6 END 	AS [GeoCodeTypeID]   
			, nl.ManifestNote
			, nl.CommonName  --
			, 'ELIGIBILITY Import'									AS [Note]


		FROM (

		    -- Phys Address
				SELECT DISTINCT 
					''							                AS [CommonName]
					, Rtrim(ISNULL(Phys_Addr_1, ''))			    AS [Address1]
					, RTRIM(ISNULL(Phys_Addr_2,''))			    	    AS [Address2]
					, RTRIM(ISNULL(Phys_Addr_City,''))							    AS [City]
					, RTRIM(ISNULL(Phys_Addr_State,''))								    AS [State]
					, RTRIM(ISNULL(LEFT(Patient_ZIP,5),''))						        AS [Zip] 
					, ISNULL(NULLIF('',''),'0.0')			AS [Latitude]
					, ISNULL(NULLIF('',''),'0.0')		AS [Longitude]
					, ISNULL('','')							AS [ManifestNote]
			
				FROM etl.Eligibility_Raw
				WHERE [ETL].[Import_GetLocationID]('',RTRIM(ISNULL(Phys_Addr_1,'')),RTRIM(ISNULL(Phys_Addr_2, '')),RTRIM(ISNULL(Phys_Addr_City,'')),RTRIM(ISNULL(Phys_Addr_State,'')),RTRIM(ISNULL(Left(Patient_ZIP,5),''))) IS NULL

				UNION 

             -- Mailing Address 
				SELECT DISTINCT 
					''							                AS [CommonName]
					, Rtrim(ISNULL(Mailing_Addr_1, ''))			    AS [Address1]
					, RTRIM(ISNULL(Mailing_Addr_2,''))			    	    AS [Address2]
					, RTRIM(ISNULL(Mailing_Addr_City,''))							    AS [City]
					, RTRIM(ISNULL(Mailing_Addr_State,''))								    AS [State]
					, RTRIM(ISNULL(LEFT(Mailing_Addr_ZIP,5),''))						        AS [Zip] 
					, ISNULL(NULLIF('',''),'0.0')			AS [Latitude]
					, ISNULL(NULLIF('',''),'0.0')		AS [Longitude]
					, ISNULL('','')							AS [ManifestNote]
			
				FROM etl.Eligibility_Raw
				WHERE [ETL].[Import_GetLocationID]('',RTRIM(ISNULL(Mailing_Addr_1,'')),RTRIM(ISNULL(Mailing_Addr_2, '')),RTRIM(ISNULL(Mailing_Addr_City,'')),RTRIM(ISNULL(Mailing_Addr_State,'')),RTRIM(ISNULL(Left(Mailing_Addr_ZIP,5),''))) IS NULL


			) nl ;

			
  -- Set @RowCount for the last insert statement 
          SET @Locationrow= @@ROWCOUNT ;

-------------------------------------------------------------------------------------------------------
-- CUSTOMER INSERT AND UPDATE BY BATCH
--------------------------------------------------------------------------------------------------------
 -- Batch count holder
DECLARE @r INT;
    SET @r = 1 ; 


--- Maping to Update Datamapping after inserting new Customers
 CREATE TABLE #InsertedCustomerID 
    (
          [CustomerID] int 
		, [ImportedID]   int
	);	

CREATE CLUSTERED INDEX ix_tempCIndexInsertedCustomerID ON #InsertedCustomerID (CustomerID)


-- TO HOLD LIST OF IDS TO IMPORT 
CREATE TABLE  #ApprovedIDs  (ID int)
CREATE CLUSTERED INDEX ix_tempCIndexApprovedID ON #ApprovedIDs (ID)



-- Create Table to hold Datamappings For ID's
CREATE TABLE #DATAMapping  
	(
	    ID int,
		CustomerID INT NULL,
		GenderID INT NULL,		
		LocationID INT NULL,
		MailLocationID INT NULL,
		ClientPlanID  INT NULL,
		ImportID INT NULL,
		BenefitGroupID INT NULL
	);

CREATE CLUSTERED INDEX ix_tempCIndexDataMappingID ON #DATAMapping  (ID) 
CREATE INDEX ix_tempIndexDataMappingCustomerID ON  #DATAMapping  (CustomerID ) 


WHILE @r > 0
BEGIN
 
  BEGIN TRANSACTION

    TRUNCATE TABLE #InsertedCustomerID;
	TRUNCATE TABLE #ApprovedIDs;
	TRUNCATE TABLE #DATAMapping; 


	
--Grab ID's Approved and ready to be processed
	PRINT 'Select Approved ETL_Process_Flag : ' + CONVERT(VARCHAR(30), GETDATE(), 121)
	
	INSERT INTO #ApprovedIDs 
		SELECT TOP 10000 ID FROM etl.Eligibility_Raw  WHERE ETL_Process_Flag = 1 ;

		


----------------------------------------------------------------------------------------
-- 3 diffrent ways to check if a customer record is already in Reveal  OPS-1008
----------------------------------------------------------------------------------------


WITH CustomerIDs AS (
-- A
 SELECT E.ID, C.[CustomerID]
 FROM Customers C with(nolock)
 INNER JOIN Etl.Eligibility_Raw E ON e.MTM_MemberID = C.ImportedID
 WHERE E.ID IN (SELECT ID FROM  #ApprovedIDs) 

 UNION
-- B
 SELECT E.ID, C.[CustomerID]
 FROM Etl.Eligibility_Raw E
 INNER JOIN ClientPlans CP with(nolock) on E.Plan_Code = CP.PlanCode
 INNER JOIN Customers C with(nolock) on E.Medicaid_ID = C.MedicalNumber AND ImportedID = 0
 WHERE E.ID IN (SELECT ID FROM  #ApprovedIDs) 
 UNION
-- C
SELECT E.ID, C.[CustomerID]
 FROM Etl.Eligibility_Raw E
 INNER JOIN ClientPlans CP with(nolock) on E.Plan_Code = CP.PlanCode
 INNER JOIN Customers C with(nolock) on E.Last_Name = C.LastName AND E.First_Name = C.FirstName AND E.DOB = C.BirthDate and e.Medicaid_ID <> c.MedicalNumber And c.ImportedID = 0
 INNER JOIN dbo.Genders G with(nolock) on E.Gender = G.Description
  WHERE E.ID IN (SELECT ID FROM  #ApprovedIDs) 
 )


	INSERT INTO #DATAMapping
		SELECT DISTINCT
		     [ID]             =  E.ID
			,[CustomerID]     =  CTE.Customerid
			,[GenderID]       =  de.Import_GetGenderID(E.Gender) 
			,[LocationID]     =  [ETL].[Import_GetLocationID]('',RTRIM(ISNULL(Phys_Addr_1,'')),RTRIM(ISNULL(Phys_Addr_2, '')),RTRIM(ISNULL(Phys_Addr_City,'')),RTRIM(ISNULL(Phys_Addr_State,'')),RTRIM(ISNULL(Left(Patient_ZIP,5),'')))
			,[MailLocationID] =  [ETL].[Import_GetLocationID]('',RTRIM(ISNULL(Mailing_Addr_1,'')),RTRIM(ISNULL(Mailing_Addr_2, '')),RTRIM(ISNULL(Mailing_Addr_City,'')),RTRIM(ISNULL(Mailing_Addr_State,'')),RTRIM(ISNULL(Left(Mailing_Addr_ZIP,5),'')))
			,[ClientPlanID]   =  [etl].[Import_GetClientPlanID](E.Plan_Code)
			,[ImportID]       =  E.MTM_MemberID
			,[BenefitGroupID] =  [etl].[Import_BenefitGroupID](E.Elig_Group,E.Plan_Code)
		   
		
		FROM Etl.Eligibility_Raw E 
  LEFT  JOIN CustomerIDs CTE on E.ID = CTE.ID
       WHERE E.ID IN (SELECT ID FROM  #ApprovedIDs) ;




-- UPDATE RAW TABLE WITH IDS, NOTE We do have duplicate CustomerIDs   GK***Need to update with min(customerID)

  UPDATE E
      
	  SET 
	       [CustomerID]   = DM.CustomerID
		  ,[GenderID]     = DM.GenderID
		  ,[LocationID]   = DM.LocationID
		  ,[ClientPlanID] = DM.ClientPlanID 
		  ,[BenefitGroupID]= DM.BenefitGroupID
		  
	  FROM  Etl.Eligibility_Raw E 
INNER JOIN  #DATAMapping DM ON E.ID = DM.ID
;

 


------------------------------------------------------------------------------------------------------------------------------------------
-- SEE IF CUSTOMERS HAVE A CUSTOMERS_EDITLOG  RECORD, IF NOT ADD CURRENT RECORD IN dbo.CUSTOMERS TO EDIT_LOG BEFORE WE MAKE A CHANGE  OPS-373
------------------------------------------------------------------------------------------------------------------------------------------
 
   
INSERT [dbo].[Customers_EditLog] ([CustomerID],[UserID],[EditTime_UTC],[BirthDate],[CustomerNumber],[DefaultResHasReturnTrip],[DefaultResAttendants]
                                 ,[DefaultResCompanions],[DefaultResLocationID],[DefaultResNumberofSubDays],[DefaultResTypeID],[DefaultResMaxRideSeconds]
								 ,[Description],[DriverNote],[IsVerified],[MailLocationID],[Note],[OpDefaultResFareTypeID],[OpDefaultResFundingTypeID],[OpDefaultResPassengerTypeID]
								 ,[OpDefaultResServiceID],[OpDefaultResTripPurposeID],[OpDefaultResOverrideTypeID],[CommonLocations],[Contacts],[EmergencyContacts],[AsExtraLoadSeconds]
								 ,[AsExtraUnloadSeconds],[Active],[EligibilityEndDate],[GenderID],[OPDefaultDropDownGeneric1ID],[OPDefaultDropDownGeneric2ID],[OPDefaultDropDownGeneric3ID]
								 ,[OPDefaultDropDownGeneric4ID],[OPDefaultDropDownGeneric5ID],[SSN],[LanguageID],[MedicalNumber],[ReservationNotification],[CustomerRaceID],[HomeLocationID]
								 ,[VoiceNotificationNumber],[TextNotificationNumber],[EmailAddress],[CustomCheck1],[CustomCheck2],[CustomCheck3],[CustomCheck4],[CustomCheck5],[CustomCheck6],[CustomCheck7]
								 ,[CustomCheck8],[CustomCheck9],[CustomField1],[CustomField2],[CustomField3],[CustomField4],[CustomField5],[CustomField6],[CustomField7],[CustomField8],[CustomField9],[CustomField10],[CustomField11],[CustomField12],[CustomField13],[CustomField14],[CustomField15]				  
                                 ,[CustomField16],[CustomField17],[CustomField18],[CustomField19],[CustomField20],[OptInVoiceNotifications],[OptInEmailNotifications],[OptInTextNotifications]				   
                                 ,[LastVerifiedUserID],[LastVerifiedDateTime_UTC],[OptInGeneralNotifications],[OptInReminderNotifications],[OptInArrivingNotifications]					   
                                 ,[ProviderID],[OpDefaultResModeID],[ClientPlanID],[Weight],[Passphrase],[HighRiskOfFraudExpiration]		  			   
                                 ,[Height],[Created_UTC],[PayeeID],[DefaultResRequiresAttendant],[FirstName],[LastName], [OpDefaultResLevelOfServiceID],[OptInConfirmationNotifications],[DefaultModeLock],[OptInInformativeNotifications]  									   
                                 ,[OptInPromotionalNotifications] )

 SELECT  DISTINCT
	           [CustomerID]                      = C.CustomerID
              ,[UserID]					         = 6
              ,[EditTime_UTC]			         = GETUTCDATE()
              ,[BirthDate]				         = C.BirthDate
              ,[CustomerNumber]			         = C.CustomerNumber
              ,[DefaultResHasReturnTrip]         = C.DefaultResHasReturnTrip
              ,[DefaultResAttendants]	         = C.DefaultResAttendants
              ,[DefaultResCompanions]	         = C.DefaultResCompanions
              ,[DefaultResLocationID]	         = C.DefaultResLocationID
              ,[DefaultResNumberofSubDays]	     = C.DefaultResNumberOfSubDays
              ,[DefaultResTypeID]			     = C.DefaultResTypeID
              ,[DefaultResMaxRideSeconds]	     = C.DefaultResMaxRideSeconds
              ,[Description]				     = C.[Description]
              ,[DriverNote]					     = C.[DriverNote]
              ,[IsVerified]					     = C.[IsVerified]
              ,[MailLocationID]				     = C.[MailLocationID]
              ,[Note]						     = C.[Note]
              ,[OpDefaultResFareTypeID]		     = C.[OpDefaultResFareTypeID]
              ,[OpDefaultResFundingTypeID]	     = C.[OpDefaultResFundingTypeID]
              ,[OpDefaultResPassengerTypeID]     = C.[OpDefaultResPassengerTypeID]
              ,[OpDefaultResServiceID]		     = C.[OpDefaultResServiceID]
              ,[OpDefaultResTripPurposeID]	     = C.[OpDefaultResTripPurposeID]
              ,[OpDefaultResOverrideTypeID]	     = C.[OpDefaultResOverrideTypeID]
              ,[CommonLocations]			     = 'N/A'
              ,[Contacts]					     = 'N/A'
              ,[EmergencyContacts]			     = ''
              ,[AsExtraLoadSeconds]			     = C.[ASExtraLoadSeconds]
              ,[AsExtraUnloadSeconds]		     = C.[AsExtraUnloadSeconds]
              ,[Active]						     = C.[Active]
              ,[EligibilityEndDate]			     = C.[EligibilityEndDate]
              ,[GenderID]					     = C.[GenderID]
              ,[OPDefaultDropDownGeneric1ID]     = C.[OPDefaultDropDownGeneric1ID]
              ,[OPDefaultDropDownGeneric2ID]     = C.[OPDefaultDropDownGeneric2ID]
              ,[OPDefaultDropDownGeneric3ID]     = C.[OPDefaultDropDownGeneric3ID]
              ,[OPDefaultDropDownGeneric4ID]     = C.[OPDefaultDropDownGeneric4ID]
              ,[OPDefaultDropDownGeneric5ID]     = C.[OPDefaultDropDownGeneric5ID]
              ,[SSN]						     = C.[SSN]
              ,[LanguageID]					     = C.[LanguageID]
              ,[MedicalNumber]				     = C.[MedicalNumber]
              ,[ReservationNotification]	     = C.[ReservationNotification]
              ,[CustomerRaceID]				     = C.[CustomerRaceID]
              ,[HomeLocationID]				     = C.[HomeLocationID]
              ,[VoiceNotificationNumber]	     = C.[VoiceNotificationNumber]
              ,[TextNotificationNumber]		     = C.[TextNotificationNumber]
              ,[EmailAddress]				     = C.[EmailAddress]
              ,[CustomCheck1]				     = C.[CustomCheck1]
              ,[CustomCheck2]				     = C.[CustomCheck2]
              ,[CustomCheck3]				     = C.[CustomCheck3]
              ,[CustomCheck4]				     = C.[CustomCheck4]
              ,[CustomCheck5]				     = C.[CustomCheck5]
              ,[CustomCheck6]				     = C.[CustomCheck6]
              ,[CustomCheck7]				     = C.[CustomCheck7]
              ,[CustomCheck8]				     = C.[CustomCheck8]
              ,[CustomCheck9]				     = C.[CustomCheck9]
              ,[CustomField1]				     = C.[CustomField1]
              ,[CustomField2]				     = C.[CustomField2]
              ,[CustomField3]				     = C.[CustomField3]
              ,[CustomField4]				     = C.[CustomField4]
              ,[CustomField5]				     = C.[CustomField5]
              ,[CustomField6]				     = C.[CustomField6]
              ,[CustomField7]				     = C.[CustomField7]
              ,[CustomField8]				     = C.[CustomField8]
              ,[CustomField9]				     = C.[CustomField9]
              ,[CustomField10]				     = C.[CustomField10]
              ,[CustomField11]				     = C.[CustomField11]
              ,[CustomField12]				     = C.[CustomField12]
              ,[CustomField13]				     = C.[CustomField13]
              ,[CustomField14]				     = C.[CustomField14]
              ,[CustomField15]				     = C.[CustomField15]
              ,[CustomField16]				     = C.[CustomField16]
              ,[CustomField17]				     = C.[CustomField17]
              ,[CustomField18]				     = C.[CustomField18]
              ,[CustomField19]				     = C.[CustomField19]
              ,[CustomField20]				     = C.[CustomField20]
              ,[OptInVoiceNotifications]	     = C.[OptInVoiceNotifications]
              ,[OptInEmailNotifications]	     = C.[OptInEmailNotifications]
              ,[OptInTextNotifications]		     = C.[OptInTextNotifications]
              ,[LastVerifiedUserID]			     = C.[LastVerifiedUserID]
              ,[LastVerifiedDateTime_UTC]	     = C.[LastVerifiedDateTime_UTC]
              ,[OptInGeneralNotifications]	     = C.[OptInGeneralNotifications]
              ,[OptInReminderNotifications]	     = C.[OptInReminderNotifications]
              ,[OptInArrivingNotifications]	     = C.[OptInArrivingNotifications]
              ,[ProviderID]					     = C.[ProviderID]
              ,[OpDefaultResModeID]			     = C.[OpDefaultResModeID]
              ,[ClientPlanID]				     = C.[ClientPlanID]
              ,[Weight]						     = C.[Weight]
              ,[Passphrase]					     = C.[Passphrase]
              ,[HighRiskOfFraudExpiration]	     = C.[HighRiskOfFraudExpiration]
              ,[Height]						     = C.[Height]
              ,[Created_UTC]				     = C.[Created_UTC]
              ,[PayeeID]					     = C.[PayeeID]
              ,[DefaultResRequiresAttendant]	 = C.[DefaultResRequiresAttendant]
              ,[FirstName]						 = C.[FirstName]
              ,[LastName]						 = C.[LastName]
              ,[OpDefaultResLevelOfServiceID]	 = C.[OpDefaultResLevelOfServiceID]
              ,[OptInConfirmationNotifications]	 = C.[OptInConfirmationNotifications]
              ,[DefaultModeLock]				 = C.[DefaultModeLock]
              ,[OptInInformativeNotifications]	 = C.[OptInInformativeNotifications]
              ,[OptInPromotionalNotifications]	 = C.[OptInPromotionalNotifications]

	  FROM [Customers_EditLog] CE
RIGHT JOIN #DATAMapping DM ON CE.CustomerID = DM.CustomerID
INNER JOIN CUSTOMERS C ON DM.CustomerID = C.CustomerID   
     WHERE CE.CustomerID IS NULL
	   AND DM.ClientPlanID IS NOT NULL ;





------------------------------------------------------------------------------
-- UPDATE CUSTOMERS , WILL NEED TO FILTER OUT ANY THAT DO NOT MATCH ON PLANCODE  OPS- 1013
------------------------------------------------------------------------------
  -- Need to add records to Customers_EditLog.  Tsql Update will always update the target, even if the vaules are the same from source/target. We might need to come back
  -- and change this to a Except statment also. Doing a where clause with all the fields will be slow. 


    Declare @UpdatedCustomerIDs  table (CustomerID int) 
	DELETE @UpdatedCustomerIDs 

  
  UPDATE CUSTOMERS 

  SET

       [ImportedID]                    = CASE WHEN [ImportedID] = 0 THEN E.[MTM_MemberID] ELSE [ImportedID] END 
      ,[BirthDate]					   = E.DOB
      ,[MedicalNumber]				   = E.Medicaid_ID  
      ,[HealthCareID]				   = E.[HealthCare_ID]
      ,[FirstName]					   = E.[First_Name]
      ,[LastName]					   = E.[Last_Name]
      ,[MiddleInitial]				   = E.[MI]
      ,[MiscellaneousID]			   = E.[Misc_ID]
      ,[MemberRegion]				   = E.[Member_Region]
	  ,[Description]                   = CASE
	                                            WHEN E.[MI] IS NOT NULL 
												THEN  CONCAT(E.[Last_Name],',',' ',E.[First_name],' ',E.[MI])
												ELSE  CONCAT(E.[Last_Name],',',' ',E.[First_name])
										 	 END 
  

OUTPUT inserted.CustomerID   INTO @UpdatedCustomerIDs													
													
        FROM  Customers C WITH(NOLOCK)		
  INNER JOIN #DATAMapping DM ON C.CustomerID = DM.CustomerID
  INNER JOIN Etl.Eligibility_Raw E on E.ID = DM.ID 
       WHERE DM.[ClientPlanID] IS NOT NULL  
         AND DM.CustomerID IS NOT NULL; 

  SET @CustomerUpdateRowCount =  @CustomerUpdateRowCount + @@ROWCOUNT;

----------------------------------------------------------------------------------------------------------------------
---  Add Updated records to Customers_EditLog  OPS-373
----------------------------------------------------------------------------------------------------------------------


   
INSERT [dbo].[Customers_EditLog] ([CustomerID],[UserID],[EditTime_UTC],[BirthDate],[CustomerNumber],[DefaultResHasReturnTrip],[DefaultResAttendants]
                                 ,[DefaultResCompanions],[DefaultResLocationID],[DefaultResNumberofSubDays],[DefaultResTypeID],[DefaultResMaxRideSeconds]
								 ,[Description],[DriverNote],[IsVerified],[MailLocationID],[Note],[OpDefaultResFareTypeID],[OpDefaultResFundingTypeID],[OpDefaultResPassengerTypeID]
								 ,[OpDefaultResServiceID],[OpDefaultResTripPurposeID],[OpDefaultResOverrideTypeID],[CommonLocations],[Contacts],[EmergencyContacts],[AsExtraLoadSeconds]
								 ,[AsExtraUnloadSeconds],[Active],[EligibilityEndDate],[GenderID],[OPDefaultDropDownGeneric1ID],[OPDefaultDropDownGeneric2ID],[OPDefaultDropDownGeneric3ID]
								 ,[OPDefaultDropDownGeneric4ID],[OPDefaultDropDownGeneric5ID],[SSN],[LanguageID],[MedicalNumber],[ReservationNotification],[CustomerRaceID],[HomeLocationID]
								 ,[VoiceNotificationNumber],[TextNotificationNumber],[EmailAddress],[CustomCheck1],[CustomCheck2],[CustomCheck3],[CustomCheck4],[CustomCheck5],[CustomCheck6],[CustomCheck7]
								 ,[CustomCheck8],[CustomCheck9],[CustomField1],[CustomField2],[CustomField3],[CustomField4],[CustomField5],[CustomField6],[CustomField7],[CustomField8],[CustomField9],[CustomField10],[CustomField11],[CustomField12],[CustomField13],[CustomField14],[CustomField15]				  
                                 ,[CustomField16],[CustomField17],[CustomField18],[CustomField19],[CustomField20],[OptInVoiceNotifications],[OptInEmailNotifications],[OptInTextNotifications]				   
                                 ,[LastVerifiedUserID],[LastVerifiedDateTime_UTC],[OptInGeneralNotifications],[OptInReminderNotifications],[OptInArrivingNotifications]					   
                                 ,[ProviderID],[OpDefaultResModeID],[ClientPlanID],[Weight],[Passphrase],[HighRiskOfFraudExpiration]		  			   
                                 ,[Height],[Created_UTC],[PayeeID],[DefaultResRequiresAttendant],[FirstName],[LastName], [OpDefaultResLevelOfServiceID],[OptInConfirmationNotifications],[DefaultModeLock],[OptInInformativeNotifications]  									   
                                 ,[OptInPromotionalNotifications] )

 SELECT  DISTINCT
	           [CustomerID]                      = C.CustomerID
              ,[UserID]					         = 6
              ,[EditTime_UTC]			         = GETUTCDATE()
              ,[BirthDate]				         = C.BirthDate
              ,[CustomerNumber]			         = C.CustomerNumber
              ,[DefaultResHasReturnTrip]         = C.DefaultResHasReturnTrip
              ,[DefaultResAttendants]	         = C.DefaultResAttendants
              ,[DefaultResCompanions]	         = C.DefaultResCompanions
              ,[DefaultResLocationID]	         = C.DefaultResLocationID
              ,[DefaultResNumberofSubDays]	     = C.DefaultResNumberOfSubDays
              ,[DefaultResTypeID]			     = C.DefaultResTypeID
              ,[DefaultResMaxRideSeconds]	     = C.DefaultResMaxRideSeconds
              ,[Description]				     = C.[Description]
              ,[DriverNote]					     = C.[DriverNote]
              ,[IsVerified]					     = C.[IsVerified]
              ,[MailLocationID]				     = C.[MailLocationID]
              ,[Note]						     = C.[Note]
              ,[OpDefaultResFareTypeID]		     = C.[OpDefaultResFareTypeID]
              ,[OpDefaultResFundingTypeID]	     = C.[OpDefaultResFundingTypeID]
              ,[OpDefaultResPassengerTypeID]     = C.[OpDefaultResPassengerTypeID]
              ,[OpDefaultResServiceID]		     = C.[OpDefaultResServiceID]
              ,[OpDefaultResTripPurposeID]	     = C.[OpDefaultResTripPurposeID]
              ,[OpDefaultResOverrideTypeID]	     = C.[OpDefaultResOverrideTypeID]
              ,[CommonLocations]			     = 'N/A'
              ,[Contacts]					     = 'N/A'
              ,[EmergencyContacts]			     = ''
              ,[AsExtraLoadSeconds]			     = C.[ASExtraLoadSeconds]
              ,[AsExtraUnloadSeconds]		     = C.[AsExtraUnloadSeconds]
              ,[Active]						     = C.[Active]
              ,[EligibilityEndDate]			     = C.[EligibilityEndDate]
              ,[GenderID]					     = C.[GenderID]
              ,[OPDefaultDropDownGeneric1ID]     = C.[OPDefaultDropDownGeneric1ID]
              ,[OPDefaultDropDownGeneric2ID]     = C.[OPDefaultDropDownGeneric2ID]
              ,[OPDefaultDropDownGeneric3ID]     = C.[OPDefaultDropDownGeneric3ID]
              ,[OPDefaultDropDownGeneric4ID]     = C.[OPDefaultDropDownGeneric4ID]
              ,[OPDefaultDropDownGeneric5ID]     = C.[OPDefaultDropDownGeneric5ID]
              ,[SSN]						     = C.[SSN]
              ,[LanguageID]					     = C.[LanguageID]
              ,[MedicalNumber]				     = C.[MedicalNumber]
              ,[ReservationNotification]	     = C.[ReservationNotification]
              ,[CustomerRaceID]				     = C.[CustomerRaceID]
              ,[HomeLocationID]				     = C.[HomeLocationID]
              ,[VoiceNotificationNumber]	     = C.[VoiceNotificationNumber]
              ,[TextNotificationNumber]		     = C.[TextNotificationNumber]
              ,[EmailAddress]				     = C.[EmailAddress]
              ,[CustomCheck1]				     = C.[CustomCheck1]
              ,[CustomCheck2]				     = C.[CustomCheck2]
              ,[CustomCheck3]				     = C.[CustomCheck3]
              ,[CustomCheck4]				     = C.[CustomCheck4]
              ,[CustomCheck5]				     = C.[CustomCheck5]
              ,[CustomCheck6]				     = C.[CustomCheck6]
              ,[CustomCheck7]				     = C.[CustomCheck7]
              ,[CustomCheck8]				     = C.[CustomCheck8]
              ,[CustomCheck9]				     = C.[CustomCheck9]
              ,[CustomField1]				     = C.[CustomField1]
              ,[CustomField2]				     = C.[CustomField2]
              ,[CustomField3]				     = C.[CustomField3]
              ,[CustomField4]				     = C.[CustomField4]
              ,[CustomField5]				     = C.[CustomField5]
              ,[CustomField6]				     = C.[CustomField6]
              ,[CustomField7]				     = C.[CustomField7]
              ,[CustomField8]				     = C.[CustomField8]
              ,[CustomField9]				     = C.[CustomField9]
              ,[CustomField10]				     = C.[CustomField10]
              ,[CustomField11]				     = C.[CustomField11]
              ,[CustomField12]				     = C.[CustomField12]
              ,[CustomField13]				     = C.[CustomField13]
              ,[CustomField14]				     = C.[CustomField14]
              ,[CustomField15]				     = C.[CustomField15]
              ,[CustomField16]				     = C.[CustomField16]
              ,[CustomField17]				     = C.[CustomField17]
              ,[CustomField18]				     = C.[CustomField18]
              ,[CustomField19]				     = C.[CustomField19]
              ,[CustomField20]				     = C.[CustomField20]
              ,[OptInVoiceNotifications]	     = C.[OptInVoiceNotifications]
              ,[OptInEmailNotifications]	     = C.[OptInEmailNotifications]
              ,[OptInTextNotifications]		     = C.[OptInTextNotifications]
              ,[LastVerifiedUserID]			     = C.[LastVerifiedUserID]
              ,[LastVerifiedDateTime_UTC]	     = C.[LastVerifiedDateTime_UTC]
              ,[OptInGeneralNotifications]	     = C.[OptInGeneralNotifications]
              ,[OptInReminderNotifications]	     = C.[OptInReminderNotifications]
              ,[OptInArrivingNotifications]	     = C.[OptInArrivingNotifications]
              ,[ProviderID]					     = C.[ProviderID]
              ,[OpDefaultResModeID]			     = C.[OpDefaultResModeID]
              ,[ClientPlanID]				     = C.[ClientPlanID]
              ,[Weight]						     = C.[Weight]
              ,[Passphrase]					     = C.[Passphrase]
              ,[HighRiskOfFraudExpiration]	     = C.[HighRiskOfFraudExpiration]
              ,[Height]						     = C.[Height]
              ,[Created_UTC]				     = C.[Created_UTC]
              ,[PayeeID]					     = C.[PayeeID]
              ,[DefaultResRequiresAttendant]	 = C.[DefaultResRequiresAttendant]
              ,[FirstName]						 = C.[FirstName]
              ,[LastName]						 = C.[LastName]
              ,[OpDefaultResLevelOfServiceID]	 = C.[OpDefaultResLevelOfServiceID]
              ,[OptInConfirmationNotifications]	 = C.[OptInConfirmationNotifications]
              ,[DefaultModeLock]				 = C.[DefaultModeLock]
              ,[OptInInformativeNotifications]	 = C.[OptInInformativeNotifications]
              ,[OptInPromotionalNotifications]	 = C.[OptInPromotionalNotifications]

	  FROM CUSTOMERS C
INNER JOIN @UpdatedCustomerIDs UCI ON C.CustomerID = UCI.CustomerID;
 
    
	

   
---------------------------------------------------------------------------------------------------------------------------------------------------
-- INSERT NEW CUSTOMER RECORD OPS-1012
---------------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO CUSTOMERS
( [Description]      															
,[ImportedID],[BirthDate],[IsVerified],[CustomerNumber],[MailLocationID],[Note],[DefaultResLocationID],[DriverNote],[DefaultResTypeID],[DefaultResHasReturnTrip]	   		   
,[DefaultResNumberOfSubDays],[DefaultResAttendants],[DefaultResCompanions],[DefaultResMaxRideSeconds],[OpDefaultResPassengerTypeID] ,[OpDefaultResFundingTypeID]		
,[OpDefaultResFareTypeID],[OpDefaultResServiceID],[OpDefaultResTripPurposeID],[OpDefaultResOverrideTypeID],[ASExtraLoadSeconds],[AsExtraUnloadSeconds],[Active]					   
,[EligibilityEndDate],[GenderID],[MedicalNumber],[ProviderCustomerNumber],[OPDefaultDropDownGeneric1ID],[OPDefaultDropDownGeneric2ID],[OPDefaultDropDownGeneric3ID] 				   
,[OPDefaultDropDownGeneric4ID],[OPDefaultDropDownGeneric5ID],[LanguageID],[SSN],[ReservationNotification],[CustomerRaceID],[HomeLocationID],[PortalPassword]				   				   
,[VoiceNotificationNumber],[TextNotificationNumber],[EmailAddress],[CustomCheck1],[CustomCheck2],[CustomCheck3],[CustomCheck4]						   
,[CustomCheck5],[CustomCheck6],[CustomCheck7],[CustomCheck8],[CustomCheck9],[CustomField1],[CustomField2],[CustomField3],[CustomField4],[CustomField5]			   
,[CustomField6],[CustomField7],[CustomField8],[CustomField9],[CustomField10],[CustomField11],[CustomField12],[CustomField13],[CustomField14],[CustomField15]				  
,[CustomField16],[CustomField17],[CustomField18],[CustomField19],[CustomField20],[OptInVoiceNotifications],[OptInEmailNotifications],[OptInTextNotifications]				   
,[LastVerifiedUserID],[LastVerifiedDateTime_UTC],[OptInGeneralNotifications],[OptInReminderNotifications],[OptInArrivingNotifications],[ProviderID]					   
,[ClientPlanID],[HealthCareID],[FirstName],[LastName],[MiddleInitial],[MiscellaneousID],[MemberRegion],[OpDefaultResModeID],[Weight],[Passphrase],[HighRiskOfFraudExpiration]		  			   
,[Height],[Created_UTC],[PayeeID],[DefaultResRequiresAttendant],[OpDefaultResLevelOfServiceID],[OptInConfirmationNotifications],[DefaultModeLock],[OptInInformativeNotifications]  									   
,[OptInPromotionalNotifications] 	)


OUTPUT inserted.CustomerID,inserted.ImportedID INTO  #InsertedCustomerID  -- we need to keep inserted CustomerID and Update Mapping & Customer_EditLog

SELECT 
  DISTINCT   
     --  [CustomerID]                    =  DM.CustomerID
       [Description]                   =   CASE
	                                            WHEN [MI] IS NOT NULL 
												THEN  CONCAT([Last_Name],',',' ',[First_name],' ',[MI])
												ELSE  CONCAT([Last_Name],',',' ',[First_name])
										 	 END  
      ,[ImportedID]                    =  E.[MTM_MemberID]
      ,[BirthDate]					   =  E.DOB
      ,[IsVerified]					   =  0
      ,[CustomerNumber]				   =  0    -- 0 is a placeholder will update with CustomerID
      ,[MailLocationID]				   =  CASE WHEN Dm.[MailLocationID] = 0 THEN Dm.LocationID ELSE Dm.[MailLocationID] END
      ,[Note]						   = ''
      ,[DefaultResLocationID]		   =  Dm.[LocationID]
      ,[DriverNote]					   = ''
      ,[DefaultResTypeID]			   =  1
      ,[DefaultResHasReturnTrip]	   =  1
      ,[DefaultResNumberOfSubDays]	   =  14
      ,[DefaultResAttendants]		   =  0
      ,[DefaultResCompanions]		   =  0
      ,[DefaultResMaxRideSeconds]	   =  3600
      ,[OpDefaultResPassengerTypeID]   =  0
      ,[OpDefaultResFundingTypeID]	   =  1
      ,[OpDefaultResFareTypeID]		   =  1
      ,[OpDefaultResServiceID]		   =  1
      ,[OpDefaultResTripPurposeID]	   =  0
      ,[OpDefaultResOverrideTypeID]	   =  1
      ,[ASExtraLoadSeconds]			   =  0
      ,[AsExtraUnloadSeconds]		   =  0
      ,[Active]						   =  1
      ,[EligibilityEndDate]			   =  '12/31/9999'
      ,[GenderID]					   = ISNULL(de.Import_GetGenderID(e.Gender),1)
      ,[MedicalNumber]				   = E.Medicaid_ID
      ,[ProviderCustomerNumber]		   = NULL
      ,[OPDefaultDropDownGeneric1ID]   = 0
      ,[OPDefaultDropDownGeneric2ID]   = 0
      ,[OPDefaultDropDownGeneric3ID]   = 0
      ,[OPDefaultDropDownGeneric4ID]   = 0
      ,[OPDefaultDropDownGeneric5ID]   = 0
      ,[LanguageID]					   = 1
      ,[SSN]						   = ''
      ,[ReservationNotification]	   = ''
      ,[CustomerRaceID]				   = 1
      ,[HomeLocationID]				   = ISNULL(Dm.[LocationID],0)   
      ,[PortalPassword]				   = NULL
      ,[VoiceNotificationNumber]	   = ''
      ,[TextNotificationNumber]		   = ''
      ,[EmailAddress]				   = ''
      ,[CustomCheck1]				   = 0
      ,[CustomCheck2]				   = 0
      ,[CustomCheck3]				   = 0
      ,[CustomCheck4]				   = 0
      ,[CustomCheck5]				   = 0
      ,[CustomCheck6]				   = 0
      ,[CustomCheck7]				   = 0
      ,[CustomCheck8]				   = 0
      ,[CustomCheck9]				   = 0
      ,[CustomField1]				   = ''
      ,[CustomField2]				   = ''
      ,[CustomField3]				   = ''
      ,[CustomField4]				   = ''
      ,[CustomField5]				   = ''
      ,[CustomField6]				   = ''
      ,[CustomField7]				   = ''
      ,[CustomField8]				   = ''
      ,[CustomField9]				   = ''
      ,[CustomField10]				   = ''
      ,[CustomField11]				   = ''
      ,[CustomField12]				   = ''
      ,[CustomField13]				   = ''
      ,[CustomField14]				   = ''
      ,[CustomField15]				   = ''
      ,[CustomField16]				   = ''
      ,[CustomField17]				   = ''
      ,[CustomField18]				   = ''
      ,[CustomField19]				   = ''
      ,[CustomField20]				   = ''
      ,[OptInVoiceNotifications]	   = 0
      ,[OptInEmailNotifications]	   = 0
      ,[OptInTextNotifications]		   = 0
      ,[LastVerifiedUserID]			   = 0
      ,[LastVerifiedDateTime_UTC]	   = GETUTCDATE()
      ,[OptInGeneralNotifications]	   = 0
      ,[OptInReminderNotifications]	   = 0
      ,[OptInArrivingNotifications]	   = 0
      ,[ProviderID]					   = 1
      ,[ClientPlanID]				   = DM.[ClientPlanID]
      ,[HealthCareID]				   = E.[HealthCare_ID]
      ,[FirstName]					   = E.[First_Name]
      ,[LastName]					   = E.[Last_Name]
      ,[MiddleInitial]				   = E.[MI]
      ,[MiscellaneousID]			   = E.[Misc_ID]
      ,[MemberRegion]				   = E.[Member_Region]
      ,[OpDefaultResModeID]			   = 0
      ,[Weight]						   = NULL
      ,[Passphrase]					   = ''
      ,[HighRiskOfFraudExpiration]	   = NULL
      ,[Height]						   = NULL
      ,[Created_UTC]				   = GETUTCDATE()
      ,[PayeeID]					   = NULL
      ,[DefaultResRequiresAttendant]   = 0
      ,[OpDefaultResLevelOfServiceID]  = 0
      ,[OptInConfirmationNotifications]= 1
      ,[DefaultModeLock]			   = NULL
      ,[OptInInformativeNotifications] = 0
      ,[OptInPromotionalNotifications] = 0
	

   FROM  Etl.Eligibility_Raw E
  INNER JOIN #DATAMapping DM on E.ID = DM.ID 
  WHERE DM.[ClientPlanID] IS NOT NULL 
    AND DM.CustomerID IS NULL; 

 
  SET @CustomerInsertRowCount =  @CustomerInsertRowCount + @@ROWCOUNT;



   -- Update DataMapping with CustomerIDs Inserted

    UPDATE DM
       SET DM.CustomerID = IC.CustomerID

      FROM #DATAMapping DM
INNER JOIN #InsertedCustomerID  IC on DM.importid = IC.ImportedID;



-- UPDATE RAW TABLE WITH IDS, NOTE We do have duplicate CustomerIDs 

  UPDATE E
      
	  SET 
	       [CustomerID]   = DM.CustomerID
		  ,[GenderID]     = DM.GenderID
		  ,[LocationID]   = DM.LocationID
		  ,[ClientPlanID] = DM.ClientPlanID 
		  
	  FROM  Etl.Eligibility_Raw E 
INNER JOIN  #DATAMapping DM ON E.ID = DM.ID;



-- We need to update dbo.Customer.CustomerNumber with CustomerID 

UPDATE C
   SET CustomerNumber = C.CustomerID

         FROM #DATAMapping DM 
   INNER JOIN dbo.Customers C  on DM.CustomerID = C.CustomerID AND C.CustomerNumber = 0;  


----------------------------------------------------------------------------------------------------------------------
---  ADD INSERTED RECORDS TO dbo.Customers_EditLog -- OPS-373
----------------------------------------------------------------------------------------------------------------------


   
INSERT [dbo].[Customers_EditLog] ([CustomerID],[UserID],[EditTime_UTC],[BirthDate],[CustomerNumber],[DefaultResHasReturnTrip],[DefaultResAttendants]
                                 ,[DefaultResCompanions],[DefaultResLocationID],[DefaultResNumberofSubDays],[DefaultResTypeID],[DefaultResMaxRideSeconds]
								 ,[Description],[DriverNote],[IsVerified],[MailLocationID],[Note],[OpDefaultResFareTypeID],[OpDefaultResFundingTypeID],[OpDefaultResPassengerTypeID]
								 ,[OpDefaultResServiceID],[OpDefaultResTripPurposeID],[OpDefaultResOverrideTypeID],[CommonLocations],[Contacts],[EmergencyContacts],[AsExtraLoadSeconds]
								 ,[AsExtraUnloadSeconds],[Active],[EligibilityEndDate],[GenderID],[OPDefaultDropDownGeneric1ID],[OPDefaultDropDownGeneric2ID],[OPDefaultDropDownGeneric3ID]
								 ,[OPDefaultDropDownGeneric4ID],[OPDefaultDropDownGeneric5ID],[SSN],[LanguageID],[MedicalNumber],[ReservationNotification],[CustomerRaceID],[HomeLocationID]
								 ,[VoiceNotificationNumber],[TextNotificationNumber],[EmailAddress],[CustomCheck1],[CustomCheck2],[CustomCheck3],[CustomCheck4],[CustomCheck5],[CustomCheck6],[CustomCheck7]
								 ,[CustomCheck8],[CustomCheck9],[CustomField1],[CustomField2],[CustomField3],[CustomField4],[CustomField5],[CustomField6],[CustomField7],[CustomField8],[CustomField9],[CustomField10],[CustomField11],[CustomField12],[CustomField13],[CustomField14],[CustomField15]				  
                                 ,[CustomField16],[CustomField17],[CustomField18],[CustomField19],[CustomField20],[OptInVoiceNotifications],[OptInEmailNotifications],[OptInTextNotifications]				   
                                 ,[LastVerifiedUserID],[LastVerifiedDateTime_UTC],[OptInGeneralNotifications],[OptInReminderNotifications],[OptInArrivingNotifications]					   
                                 ,[ProviderID],[OpDefaultResModeID],[ClientPlanID],[Weight],[Passphrase],[HighRiskOfFraudExpiration]		  			   
                                 ,[Height],[Created_UTC],[PayeeID],[DefaultResRequiresAttendant],[FirstName],[LastName], [OpDefaultResLevelOfServiceID],[OptInConfirmationNotifications],[DefaultModeLock],[OptInInformativeNotifications]  									   
                                 ,[OptInPromotionalNotifications] )

 SELECT  DISTINCT
	           [CustomerID]                      = C.CustomerID
              ,[UserID]					         = 6
              ,[EditTime_UTC]			         = GETUTCDATE()
              ,[BirthDate]				         = C.BirthDate
              ,[CustomerNumber]			         = C.CustomerNumber
              ,[DefaultResHasReturnTrip]         = C.DefaultResHasReturnTrip
              ,[DefaultResAttendants]	         = C.DefaultResAttendants
              ,[DefaultResCompanions]	         = C.DefaultResCompanions
              ,[DefaultResLocationID]	         = C.DefaultResLocationID
              ,[DefaultResNumberofSubDays]	     = C.DefaultResNumberOfSubDays
              ,[DefaultResTypeID]			     = C.DefaultResTypeID
              ,[DefaultResMaxRideSeconds]	     = C.DefaultResMaxRideSeconds
              ,[Description]				     = C.[Description]
              ,[DriverNote]					     = C.[DriverNote]
              ,[IsVerified]					     = C.[IsVerified]
              ,[MailLocationID]				     = C.[MailLocationID]
              ,[Note]						     = C.[Note]
              ,[OpDefaultResFareTypeID]		     = C.[OpDefaultResFareTypeID]
              ,[OpDefaultResFundingTypeID]	     = C.[OpDefaultResFundingTypeID]
              ,[OpDefaultResPassengerTypeID]     = C.[OpDefaultResPassengerTypeID]
              ,[OpDefaultResServiceID]		     = C.[OpDefaultResServiceID]
              ,[OpDefaultResTripPurposeID]	     = C.[OpDefaultResTripPurposeID]
              ,[OpDefaultResOverrideTypeID]	     = C.[OpDefaultResOverrideTypeID]
              ,[CommonLocations]			     = 'N/A' -- Might need to come back and look at his
              ,[Contacts]					     = 'N/A' -- Might need to come back and look at his
              ,[EmergencyContacts]			     = ''
              ,[AsExtraLoadSeconds]			     = C.[ASExtraLoadSeconds]
              ,[AsExtraUnloadSeconds]		     = C.[AsExtraUnloadSeconds]
              ,[Active]						     = C.[Active]
              ,[EligibilityEndDate]			     = C.[EligibilityEndDate]
              ,[GenderID]					     = C.[GenderID]
              ,[OPDefaultDropDownGeneric1ID]     = C.[OPDefaultDropDownGeneric1ID]
              ,[OPDefaultDropDownGeneric2ID]     = C.[OPDefaultDropDownGeneric2ID]
              ,[OPDefaultDropDownGeneric3ID]     = C.[OPDefaultDropDownGeneric3ID]
              ,[OPDefaultDropDownGeneric4ID]     = C.[OPDefaultDropDownGeneric4ID]
              ,[OPDefaultDropDownGeneric5ID]     = C.[OPDefaultDropDownGeneric5ID]
              ,[SSN]						     = C.[SSN]
              ,[LanguageID]					     = C.[LanguageID]
              ,[MedicalNumber]				     = C.[MedicalNumber]
              ,[ReservationNotification]	     = C.[ReservationNotification]
              ,[CustomerRaceID]				     = C.[CustomerRaceID]
              ,[HomeLocationID]				     = C.[HomeLocationID]
              ,[VoiceNotificationNumber]	     = C.[VoiceNotificationNumber]
              ,[TextNotificationNumber]		     = C.[TextNotificationNumber]
              ,[EmailAddress]				     = C.[EmailAddress]
              ,[CustomCheck1]				     = C.[CustomCheck1]
              ,[CustomCheck2]				     = C.[CustomCheck2]
              ,[CustomCheck3]				     = C.[CustomCheck3]
              ,[CustomCheck4]				     = C.[CustomCheck4]
              ,[CustomCheck5]				     = C.[CustomCheck5]
              ,[CustomCheck6]				     = C.[CustomCheck6]
              ,[CustomCheck7]				     = C.[CustomCheck7]
              ,[CustomCheck8]				     = C.[CustomCheck8]
              ,[CustomCheck9]				     = C.[CustomCheck9]
              ,[CustomField1]				     = C.[CustomField1]
              ,[CustomField2]				     = C.[CustomField2]
              ,[CustomField3]				     = C.[CustomField3]
              ,[CustomField4]				     = C.[CustomField4]
              ,[CustomField5]				     = C.[CustomField5]
              ,[CustomField6]				     = C.[CustomField6]
              ,[CustomField7]				     = C.[CustomField7]
              ,[CustomField8]				     = C.[CustomField8]
              ,[CustomField9]				     = C.[CustomField9]
              ,[CustomField10]				     = C.[CustomField10]
              ,[CustomField11]				     = C.[CustomField11]
              ,[CustomField12]				     = C.[CustomField12]
              ,[CustomField13]				     = C.[CustomField13]
              ,[CustomField14]				     = C.[CustomField14]
              ,[CustomField15]				     = C.[CustomField15]
              ,[CustomField16]				     = C.[CustomField16]
              ,[CustomField17]				     = C.[CustomField17]
              ,[CustomField18]				     = C.[CustomField18]
              ,[CustomField19]				     = C.[CustomField19]
              ,[CustomField20]				     = C.[CustomField20]
              ,[OptInVoiceNotifications]	     = C.[OptInVoiceNotifications]
              ,[OptInEmailNotifications]	     = C.[OptInEmailNotifications]
              ,[OptInTextNotifications]		     = C.[OptInTextNotifications]
              ,[LastVerifiedUserID]			     = C.[LastVerifiedUserID]
              ,[LastVerifiedDateTime_UTC]	     = C.[LastVerifiedDateTime_UTC]
              ,[OptInGeneralNotifications]	     = C.[OptInGeneralNotifications]
              ,[OptInReminderNotifications]	     = C.[OptInReminderNotifications]
              ,[OptInArrivingNotifications]	     = C.[OptInArrivingNotifications]
              ,[ProviderID]					     = C.[ProviderID]
              ,[OpDefaultResModeID]			     = C.[OpDefaultResModeID]
              ,[ClientPlanID]				     = C.[ClientPlanID]
              ,[Weight]						     = C.[Weight]
              ,[Passphrase]					     = C.[Passphrase]
              ,[HighRiskOfFraudExpiration]	     = C.[HighRiskOfFraudExpiration]
              ,[Height]						     = C.[Height]
              ,[Created_UTC]				     = C.[Created_UTC]
              ,[PayeeID]					     = C.[PayeeID]
              ,[DefaultResRequiresAttendant]	 = C.[DefaultResRequiresAttendant]
              ,[FirstName]						 = C.[FirstName]
              ,[LastName]						 = C.[LastName]
              ,[OpDefaultResLevelOfServiceID]	 = C.[OpDefaultResLevelOfServiceID]
              ,[OptInConfirmationNotifications]	 = C.[OptInConfirmationNotifications]
              ,[DefaultModeLock]				 = C.[DefaultModeLock]
              ,[OptInInformativeNotifications]	 = C.[OptInInformativeNotifications]
              ,[OptInPromotionalNotifications]	 = C.[OptInPromotionalNotifications]

	  FROM CUSTOMERS C
INNER JOIN #InsertedCustomerID IC ON C.CustomerID = IC.CustomerID;
 
     
	 TRUNCATE TABLE #InsertedCustomerID

	
-------------------------------------------------------------------------------------------------------------------------------------------
-- CommonLocations OPS-1016
-------------------------------------------------------------------------------------------------------------------------------------------
 DECLARE @CustomerCommonLocationID TABLE ([CustomerCommonLocationID] int, CustomerID int)


INSERT INTO CustomerCommonLocations ([CustomerID],[LocationID],[Active],[Created_UTC],[CreatedUserID],[Deleted_UTC],[DeletedUserID])

 OUTPUT inserted.CustomerCommonLocationID,inserted.CustomerID INTO @CustomerCommonLocationID


 SELECT  DISTINCT  
            [CustomerID]       = C.CustomerID 
           ,[LocationID]	   = DM.LocationID
           ,[Active]           = 1      
           ,[Created_UTC]	   = GETUTCDATE()
           ,[CreatedUserID]	   = 6
           ,[Deleted_UTC]	   = NULL
           ,[DeletedUserID]	   = NULL 

	  FROM #DATAMapping DM
 LEFT JOIN CustomerCommonLocations CCL WITH(NOLOCK) ON DM.CustomerID = CCL.CustomerID AND DM.LocationID = CCL.LocationID AND CCL.Active = 1  AND CCL.Deleted_UTC IS NULL
INNER JOIN CUSTOMERS C WITH(NOLOCK) ON Dm.CustomerID = C.CustomerID
     WHERE CCL.CustomerID IS NULL
       AND DM.LocationID  != 0 ;
  


 
	  SET @CustomerCommonLocationsInsertRowCount  = @CustomerCommonLocationsInsertRowCount  + @@ROWCOUNT  ;


	--UPDATE RAW TABLE WITH CustomerCommonLocationIDs

	    UPDATE E

           SET [CustomerCommonLocationID]  = CC.CustomerCommonLocationID

         FROM ETL.Eligibility_Raw E
	INNER JOIN #DATAMapping DM ON E.ID = DM.ID 
	INNER JOIN @CustomerCommonLocationID CC on DM.CustomerID = CC.CustomerID;
 
 DELETE FROM @CustomerCommonLocationID;
 
	
--------------------------------------------------------------------------------------------------------------------------------------------------
-- CustomerContacts  OPS-1015
--------------------------------------------------------------------------------------------------------------------------------------------------

   DECLARE @CustomerContactsID TABLE ( CustomerContactID int, CustomerID int) -- To update Raw table

---- NEW PRIMARY  
 INSERT INTO [dbo].[CustomerContacts] ([CustomerContactTypeID],[CustomerID],[Order],[Description],[Active], Created_UTC,CreatedUserID
                                       ,[Deleted_UTC], [DeletedUserID])

  OUTPUT inserted.CustomerContactID,inserted.CustomerID INTO @CustomerContactsID

 SELECT 
      
	   [CustomerContactTypeID]  = 2  -- Home Phone
      ,[CustomerID]				= DM.CustomerID
      ,[Order]					= 1  -- 1
      ,[Description]			= E.Telephone
      ,[Active]					= 1
      ,[Created_UTC]			= GETUTCDATE()
      ,[CreatedUserID]			= 6
      ,[Deleted_UTC]			= NULL
      ,[DeletedUserID]			= NULL


	    FROM  Etl.Eligibility_Raw E
  INNER JOIN  #DATAMapping DM on E.ID = DM.ID  AND DM.CustomerID IS NOT NULL 
       WHERE 
  NOT EXISTS (SELECT 1 FROM CustomerContacts CC  WHERE CC.CustomerID = DM.CustomerID AND CC.[Order] = 1 AND CC.[Active] = 1 AND ISNULL(CC.DeletedUserID,0) = 0 ) 
	     AND  LEN(E.Telephone) = 10; 

    SET @CustomerContactsInsertRowCount  = @CustomerContactsInsertRowCount  + @@ROWCOUNT  ;


	
----  IF PRIMARY RECORD ALREADY IN THE DB FOR CUSTOMER  ADD RECORD AS ORDER 0 
 INSERT INTO [dbo].[CustomerContacts] ([CustomerContactTypeID],[CustomerID],[Order],[Description],[Active], Created_UTC,CreatedUserID
                                       ,[Deleted_UTC], [DeletedUserID])


 OUTPUT inserted.CustomerContactID,inserted.CustomerID INTO @CustomerContactsID

 SELECT 
      
	   [CustomerContactTypeID]  = 2  -- Home Phone
      ,[CustomerID]				= DM.CustomerID
      ,[Order]					= 0  --  0 
      ,[Description]			= E.Telephone
      ,[Active]					= 1
      ,[Created_UTC]			= GETUTCDATE()
      ,[CreatedUserID]			= 6
      ,[Deleted_UTC]			= NULL
      ,[DeletedUserID]			= NULL

	    FROM  Etl.Eligibility_Raw E
  INNER JOIN  #DATAMapping DM on E.ID = DM.ID  AND DM.CustomerID IS NOT NULL 
  
      WHERE 
	       EXISTS  (SELECT 1 FROM CustomerContacts CC  WHERE CC.CustomerID = DM.CustomerID  AND CC.[Order] = 1 AND CC.[Active] = 1 AND ISNULL(CC.DeletedUserID,0) = 0 ) 
   AND NOT EXISTS   ((SELECT 1 FROM CustomerContacts CC  WHERE CC.CustomerID = DM.CustomerID AND CC.[Description] = E.[Telephone] AND  CC.[Order] = 1 AND CC.[Active] = 1 AND ISNULL(CC.DeletedUserID,0) = 0 ) )
   AND NOT EXISTS  (SELECT 1 FROM CustomerContacts CC  WHERE CC.CustomerID = DM.CustomerID AND CC.[Description] = E.[Telephone] AND  CC.[Order] = 0 AND CC.[Active] = 1 AND ISNULL(CC.DeletedUserID,0) = 0 ) 
	          AND  LEN(E.Telephone) = 10; 

    SET @CustomerContactsInsertRowCount  = @CustomerContactsInsertRowCount  + @@ROWCOUNT  ;


	--UPDATE RAW TABLE WITH CustomerContactIDs

	    UPDATE E

           SET  [CustomerContactID] = CC.CustomerContactID

         FROM ETL.Eligibility_Raw E
	INNER JOIN #DATAMapping DM ON E.ID = DM.ID 
	INNER JOIN @CustomerContactsID CC on DM.CustomerID = CC.CustomerID;
 
 
----------------------------------------------------------------------------------------------------------
-- CustomerPlanEligibilities OPS-1014
---------------------------------------------------------------------------------------------------------
/*We have 3 criterias below To find CustomerPlanEligibilities that need to be updated. Note: Unless we clean up CustomerPlanEligibilities,
      there will be records from the source that match multiple CustomerPlanEligibilities.If no Matches are found for criteria, then we insert the record 

select top 10000 * from CustomerPlanEligibilities order by customerid 
*/


  DECLARE @CustomerPlanEligibilityID TABLE ( CustomerPlanEligibilityID int, CustomerID int,BenefitGroupID int)
  DECLARE @UpdatedCustomerPlanEligibilityID TABLE ( CustomerPlanEligibilityID int, CustomerID int ,BenefitGroupID int)
 

--Criteria A: Eligibility Expiring early

     UPDATE CPE

        SET
   --   SELECT
	                 
             [ExpirationDate]	= CASE WHEN ISNULL(E.Elig_Term_Date,'9999-12-31') < CPE.EffectiveDate THEN CAST(CPE.EffectiveDate as date) ELSE  CAST(ISNULL(E.Elig_Term_Date,'9999-12-31')as date) END        
            ,[Note]				= Concat('Eligibility updated by Eligibility Importer, updated on ' ,GETUTCDATE())        
            ,[LastEdit_UTC]		= GETUTCDATE()	
            ,[LastEditUserID]	= 6
	  
      OUTPUT inserted.CustomerPlanEligibilityID, inserted.CustomerID, inserted.BenefitGroupID INTO @UpdatedCustomerPlanEligibilityID 

	    FROM ETL.Eligibility_Raw E
   INNER JOIN #DATAMapping DM ON E.ID = DM.ID 
   INNER JOIN CustomerPlanEligibilities CPE ON DM.CustomerID =CPE.CustomerID AND DM.BenefitGroupID = CPE.BenefitGroupID AND ISNULL(E.Elig_Term_Date,'9999-12-31')  <= CPE.ExpirationDate 
	    WHERE DM.BenefitGroupID IS NOT NULL ;

		 SET @CustomerPlanEligibilitiesUpdated =  @CustomerPlanEligibilitiesUpdated + @@ROWCOUNT;

 --Criteria B: Extend Expiration date
	 
   
    UPDATE CPE
       SET
   
   --   SELECT
	                 
             [ExpirationDate]	= CAST(ISNULL(E.Elig_Term_Date,'9999-12-31') as date)          
            ,[Note]				= Concat('Eligibility updated by Eligibility Importer, updated on ' ,GETUTCDATE())        
            ,[LastEdit_UTC]		= GETUTCDATE()	
            ,[LastEditUserID]	= 6
	  
	   OUTPUT inserted.CustomerPlanEligibilityID, inserted.CustomerID , inserted.BenefitGroupID INTO @UpdatedCustomerPlanEligibilityID  

	     FROM ETL.Eligibility_Raw E
   INNER JOIN #DATAMapping DM ON E.ID = DM.ID 
   INNER JOIN CustomerPlanEligibilities CPE ON DM.CustomerID =CPE.CustomerID 
                                           AND DM.BenefitGroupID = CPE.BenefitGroupID 
                                           AND ISNULL(E.Elig_Term_Date,'9999-12-31') > CPE.ExpirationDate
                                           AND  E.Elig_Eff_Date BETWEEN CPE.EffectiveDate and (CASE WHEN CPE.ExpirationDate <> '9999-12-31' THEN (CPE.ExpirationDate+1) ELSE CPE.ExpirationDate END)

	    WHERE DM.BenefitGroupID IS NOT NULL ;

		SET @CustomerPlanEligibilitiesUpdated =  @CustomerPlanEligibilitiesUpdated + @@ROWCOUNT;



 --Criteria C: Extend Expiration date and Effective date

   UPDATE  CPE  
      SET
     -- SELECT
	       

	        
             [EffectiveDate]	= E.Elig_Eff_Date
            ,[ExpirationDate]	= Cast(ISNULL(E.Elig_Term_Date,'9999-12-31') as date)         
            ,[Note]				= Concat('Eligibility updated by Eligibility Importer, updated on ' ,GETUTCDATE())         
            ,[LastEdit_UTC]		= GETUTCDATE()	
            ,[LastEditUserID]	= 6
	   
	   OUTPUT inserted.CustomerPlanEligibilityID, inserted.CustomerID , inserted.BenefitGroupID INTO @UpdatedCustomerPlanEligibilityID  

	    FROM ETL.Eligibility_Raw E
   INNER JOIN #DATAMapping DM ON E.ID = DM.ID 
   INNER JOIN CustomerPlanEligibilities CPE ON DM.CustomerID =CPE.CustomerID AND DM.BenefitGroupID = CPE.BenefitGroupID AND ISNULL(E.Elig_Term_Date,'9999-12-31') > CPE.ExpirationDate AND  E.Elig_Eff_Date < CPE.EffectiveDate
	    WHERE DM.BenefitGroupID IS NOT NULL;
		
		SET @CustomerPlanEligibilitiesUpdated =  @CustomerPlanEligibilitiesUpdated + @@ROWCOUNT;


 -- Insert new records that do not match above criteria

		 INSERT INTO [CustomerPlanEligibilities] ([CustomerID],[EffectiveDate],[ExpirationDate],[ReceivedDate],[Note],[BenefitGroupID] 
             ,[LastEdit_UTC],[LastEditUserID] )


      OUTPUT inserted.CustomerPlanEligibilityID, inserted.CustomerID ,inserted.BenefitGroupID INTO @CustomerPlanEligibilityID  	  
  
     SELECT DISTINCT

                [CustomerID] = DM.CustomerID
               ,[EffectiveDate] = E.Elig_Eff_Date
               ,[ExpirationDate] = cast(ISNULL(E.Elig_Term_Date,'9999-12-31') as date)
               ,[ReceivedDate] = GETUTCDATE()
               ,[Note] = Concat('Eligibility Inserted by Eligibility Importer insert on ' ,GETUTCDATE())
               ,[BenefitGroupID] = DM.BenefitGroupID
               ,[LastEdit_UTC] = GETUTCDATE()
               ,[LastEditUserID] = 6
    
          FROM ETL.Eligibility_Raw E
    INNER JOIN #DATAMapping DM ON E.ID = DM.ID
     LEFT JOIN CustomerPlanEligibilities CPE ON DM.CustomerID = CPE.CustomerID
           AND DM.BenefitGroupID = CPE.BenefitGroupID
           AND (
                 (ISNULL(E.Elig_Term_Date,'9999-12-31')  <= cpe.ExpirationDate)
              OR (e.Elig_Eff_Date between cpe.EffectiveDate and (CASE WHEN CPE.ExpirationDate <> '9999-12-31' THEN CPE.ExpirationDate+1 ELSE CPE.ExpirationDate END))
              OR (e.Elig_Eff_Date <= cpe.EffectiveDate and ISNULL(E.Elig_Term_Date,'9999-12-31')  >= cpe.ExpirationDate)
                )
        WHERE DM.CustomerID IS NOT NULL
          AND DM.BenefitGroupID IS NOT NULL
          AND cpe.CustomerPlanEligibilityID is null
		 ;

  SET @CustomerPlanEligibilitiesInserted =  @CustomerPlanEligibilitiesInserted + @@ROWCOUNT;


-- Update RAW table with CustomerPlanEligibilityIDs 

  UPDATE E

     SET [CustomerPlanEligibilityID] =  CPE.CustomerPlanEligibilityID

         FROM ETL.Eligibility_Raw E
	INNER JOIN #DATAMapping DM ON E.ID = DM.ID 
	INNER JOIN @UpdatedCustomerPlanEligibilityID CPE on DM.CustomerID = CPE.CustomerID AND DM.BenefitGroupID = CPE.BenefitGroupID;



	
  UPDATE E

     SET [CustomerPlanEligibilityID] =  CPE.CustomerPlanEligibilityID

         FROM ETL.Eligibility_Raw E
	INNER JOIN #DATAMapping DM ON E.ID = DM.ID 
	INNER JOIN @CustomerPlanEligibilityID CPE on DM.CustomerID = CPE.CustomerID AND DM.BenefitGroupID = CPE.BenefitGroupID;





----------------------------------------------------------------------------------------------------------------------
-- Add Updated and Inserted CustomerPlanEligibilities records to CustomerPlanEligibilities_AuditLog   OPS- 1014
----------------------------------------------------------------------------------------------------------------------
/*  A Trigger is in place to update Auditlog with any changes,  so we dont need this at this point. 

INSERT INTO [dbo].[CustomerPlanEligibilities_AuditLog] ([CustomerPlanEligibilityID],[CustomerID],[EffectiveDate],[ExpirationDate],[Note]
                                                       ,[BenefitGroupID],[LastEdit_UTC],[LastEditUserID] )

 SELECT DISTINCT 
         [CustomerPlanEligibilityID] =   CPE.[CustomerPlanEligibilityID]
        ,[CustomerID]				 =	 CPE.[CustomerID]				
        ,[EffectiveDate]			 =	 CPE.[EffectiveDate]			
        ,[ExpirationDate]			 =	 CPE.[ExpirationDate]			
        ,[Note]						 =	 CPE.[Note]						
        ,[BenefitGroupID]			 =	 CPE.[BenefitGroupID]			
        ,[LastEdit_UTC]				 =	 CPE.[LastEdit_UTC]				
        ,[LastEditUserID]			 =	 CPE.[LastEditUserID]			

  FROM   CustomerPlanEligibilities CPE
  WHERE CustomerPlanEligibilityID IN (Select  CustomerPlanEligibilityID FROM @CustomerPlanEligibilityID UNION  Select  CustomerPlanEligibilityID FROM @UpdatedCustomerPlanEligibilityID )  
		
  DELETE FROM @CustomerPlanEligibilityID        -- Clear out for next round
  DELETE FROM @UpdatedCustomerPlanEligibilityID -- Clear out for next round
  */

--------------------------------------------------------------------------------------------------------------
-- UPDATE RAW TABLE FLAG TO 0 FOR IMPORTED RECORDS AND END LOOP
----------------------------------------------------------------------------------------------------------------


-- Update Raw table [ETL_Process_Flag]to 0 so we know it has been processed 
   UPDATE E
          SET [ETL_Process_Flag]  = 0 
         FROM [etl].[Eligibility_Raw] E
   INNER JOIN #ApprovedIDs A on E.ID = A.ID;

SET @r = @@RowCount;   

   


COMMIT TRANSACTION; -- END Loop



END;

SELECT CONCAT('LocationsInserted: ',Cast(@Locationrow as varchar (255))
            ,', CustomersInserted: ',Cast(@CustomerInsertRowCount as varchar(255))
           ,' , CustomersUpdated: ',Cast(@CustomerUpdateRowCount as varchar(255))
		   ,' ,CommonLocationsUpdate: ',Cast(@CustomerCommonLocationsUpdateRowCount as Varchar(255))
           ,' ,CommonLocationsInsert: ',Cast(@CustomerCommonLocationsInsertRowCount as Varchar(255))
		   ,' ,CustomerContactInsert: ',Cast(@CustomerContactsInsertRowCount as Varchar(255))
		   ,' ,CustomerPlanEligibilitiesInsert: ',Cast(@CustomerPlanEligibilitiesInserted  as Varchar(255))
		   ,' ,CustomerPlanEligibilitiesUpdated: ',Cast(@CustomerPlanEligibilitiesUpdated   as Varchar(255)))
			  ,0
			  ,NULL;


END TRY 
BEGIN CATCH
	IF @@ERROR > 0
	 ROLLBACK TRANSACTION;	 
	 
		SELECT NULL,-1, CONCAT('ErrorLine: ',ERROR_LINE(),', Error: ',ERROR_MESSAGE());
END CATCH;