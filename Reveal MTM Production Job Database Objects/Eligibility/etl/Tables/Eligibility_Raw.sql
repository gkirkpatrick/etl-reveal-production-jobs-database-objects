﻿CREATE TABLE [etl].[Eligibility_Raw] (
    [ID]                        BIGINT        IDENTITY (1, 1) NOT NULL,
    [MTM_MemberID]              INT           NULL,
    [Medicaid_ID]               VARCHAR (255) NULL,
    [HealthCare_ID]             VARCHAR (255) NULL,
    [Misc_ID]                   VARCHAR (255) NULL,
    [Last_Name]                 VARCHAR (255) NULL,
    [First_Name]                VARCHAR (255) NULL,
    [MI]                        VARCHAR (255) NULL,
    [DOB]                       VARCHAR (255) NULL,
    [Gender]                    VARCHAR (255) NULL,
    [Phys_Addr_1]               VARCHAR (255) NULL,
    [Phys_Addr_2]               VARCHAR (255) NULL,
    [Phys_Addr_City]            VARCHAR (255) NULL,
    [Phys_Addr_State]           VARCHAR (255) NULL,
    [Patient_ZIP]               VARCHAR (255) NULL,
    [Phy_Addr_ZIP_4]            VARCHAR (255) NULL,
    [Phys_Addr_County]          VARCHAR (255) NULL,
    [Mailing_Addr_1]            VARCHAR (255) NULL,
    [Mailing_Addr_2]            VARCHAR (255) NULL,
    [Mailing_Addr_City]         VARCHAR (255) NULL,
    [Mailing_Addr_State]        VARCHAR (255) NULL,
    [Mailing_Addr_ZIP]          VARCHAR (255) NULL,
    [Mailing_Addr_ZIP4]         VARCHAR (255) NULL,
    [Telephone]                 VARCHAR (255) NULL,
    [Elig_Eff_Date]             VARCHAR (255) NULL,
    [Elig_Term_Date]            VARCHAR (255) NULL,
    [Elig_Group]                VARCHAR (255) NULL,
    [Member_Region]             VARCHAR (6)   NULL,
    [Language_Preference]       VARCHAR (255) NULL,
    [Spend_Down]                VARCHAR (255) NULL,
    [Plan_Code]                 VARCHAR (255) NULL,
    [CreatedDate]               VARCHAR (255) NULL,
    [Received]                  VARCHAR (255) NULL,
    [Reveal_Env]                VARCHAR (255) NULL,
    [ETL_Process_Flag]          INT           DEFAULT ((1)) NOT NULL,
    [CustomerID]                INT           NULL,
    [GenderID]                  INT           NULL,
    [LocationID]                INT           NULL,
    [ClientPlanID]              INT           NULL,
    [ImportID]                  INT           NULL,
    [BenefitGroupID]            INT           NULL,
    [CustomerPlanEligibilityID] INT           NULL,
    [CustomerContactID]         INT           NULL,
    [CustomerCommonLocationID]  INT           NULL,
    [hsh]                       AS            (hashbytes('SHA2_256',concat([MTM_MemberID],[DOB],[Medicaid_ID],[First_Name]))) PERSISTED,
    [hsh2]                      AS            (hashbytes('SHA2_256',concat([MTM_MemberID],CONVERT([date],[DOB]),[Medicaid_ID],CONVERT([nvarchar],[First_Name])))),
    [CustomerUpdate_hsh2]       AS            (hashbytes('SHA2_256',concat([MTM_MemberID],CONVERT([date],[DOB]),[Medicaid_ID],CONVERT([nvarchar],[HealthCare_ID]),CONVERT([nvarchar],[First_Name]),CONVERT([nvarchar],[Last_Name]),CONVERT([nvarchar],[MI]),CONVERT([nvarchar],[Misc_ID]),CONVERT([nvarchar],[Member_region])))),
    CONSTRAINT [PK_ETL_ELIGIBILITY_Raw_ID] PRIMARY KEY CLUSTERED ([ID] ASC)
);

GO

CREATE NONCLUSTERED INDEX [IX_Eligibility_ETL_Process_Flag] ON [etl].[Eligibility_Raw]
(
	[ETL_Process_Flag] ASC
)
INCLUDE([ID])

GO

CREATE NONCLUSTERED INDEX [IX_Eligibility_Raw_CustomerID] ON [etl].[Eligibility_Raw]
(
	[CustomerID] ASC
)

GO

CREATE NONCLUSTERED INDEX [IX_Eligibility_Raw_MTM_MemberID] ON [etl].[Eligibility_Raw]
(
	[MTM_MemberID] ASC
)

GO

CREATE NONCLUSTERED INDEX [IX_Eligibility_Raw_MTM_Plan_Code] ON [etl].[Eligibility_Raw]
(
	[Plan_Code] ASC
)